import request from '@/utils/request'

export function decorationList(query) {
  return request({
    url: `/management/decoration`,
    method: 'get',
    params: query
  })
}

export function deleteDecoration(did) {
  return request({
    url: `/management/decoration/` + did,
    method: 'delete'
  })
}

export function updateDecoration(did) {
  return request({
    url: '/management/decoration/' + did,
    method: 'put'
  })
}
