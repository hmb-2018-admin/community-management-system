import request from '@/utils/request'

export function propList() {
  return request({
    url: '/proprietor/findAll',
    method: 'get'
  })
}

export function propPageList(pageNum, pageSize) {
  return request({
    url: `/proprietor/findByPage/${pageNum}/${pageSize}`,
    method: 'get'
  })
}

export function addProp(data) {
  return request({
    url: `/proprietor/insert`,
    method: 'post',
    data
  })
}

export function updateProp(data) {
  return request({
    url: `/proprietor/update`,
    method: 'post',
    data
  })
}

export function deleteProp(pid) {
  return request({
    url: '/proprietor/delete/' + pid,
    method: 'delete'
  })
}

export function findById(id) {
  return request({
    url: '/proprietor/findById/' + id,
    method: 'get'
  })
}

export function findByName(name) {
  return request({
    url: '/proprietor/findByName/' + name,
    method: 'get'
  })
}

export function findByRoomId(roomId) {
  return request({
    url: '/proprietor/findByRoomId/' + roomId,
    method: 'get'
  })
}
