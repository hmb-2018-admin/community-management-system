import request from '@/utils/request'

export function payMentsList(query) {
  return request({
    url: `/management/payments`,
    method: 'get',
    params: query
  })
}

export function downloadExcel() {
  return request({
    url: '/management/paymentListTemplate',
    method: 'get',
    responseType: 'blob'
  })
}

export function uploadFiles(data) {
  return request({
    url: '/management/importPaymentList',
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    data
  })
}

