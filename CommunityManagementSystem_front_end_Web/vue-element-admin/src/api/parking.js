import request from '@/utils/request'

export function parkingList() {
  return request({
    url: '/management/parkingSpaces',
    method: 'get'
  })
}

export function updateParking(data) {
  return request({
    url: '/management/parkingSpace',
    method: 'put',
    params: data
  })
}

export function uploadFiles(data) {
  return request({
    url: 'management/importParkingSpace',
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    data
  })
}

