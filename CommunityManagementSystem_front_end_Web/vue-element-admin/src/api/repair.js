import request from '@/utils/request'

export function repairList(query) {
  return request({
    url: `/management/repair/`,
    method: 'get',
    params: query
  })
}

export function repairPageList(pageNum, pageSize) {
  return request({
    url: `/management/pageRepair/${pageNum}/${pageSize}`,
    method: 'get'
  })
}

export function deleteRepair(repairID) {
  return request({
    url: `/management/repair/${repairID}`,
    method: 'delete'
  })
}

export function updateRepair(repairID) {
  return request({
    url: '/management/repair/' + repairID,
    method: 'put'
  })
}
