import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/auth/login',
    method: 'post',
    data
  })
}

export function getInfo(data) {
  return request({
    url: '/public/info',
    method: 'get',
    data
  })
}

export function logout() {
  return request({
    url: '/public/logout',
    method: 'post'
  })
}

export function userList() {
  return request({
    url: '/management/users',
    method: 'get'
  })
}

export function updateUser(query) {
  return request({
    url: '/management/user',
    method: 'put',
    params: query
  })
}

// 从前API设计
// '/api/user/findAll',
// '/api/user/findById',
// '/api/user/save',
// '/api/user/update',
// '/api/user/delete',
// '/api/user/deletes',

// RESTful API
// '/api/user'      GET    == findAll
// '/api/user/id'   GET    == findById
// '/api/user'      POST   == saveOrUpdate   表单是否提交id
// '/api/user/id'   DELET  == delete

