package team.lhc.cms.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 文
 * @description user的dto
 */
@Data
public class UserDTO implements Serializable {
    private Integer uid;

    private String username;

    private String role;

    private Integer isLocked;
}
