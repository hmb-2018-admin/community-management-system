package team.lhc.cms.dto;


import lombok.Data;

import java.io.Serializable;

/**
 * @author 文
 * @description 合法停车信息对象实体类。若对象为空时,可能车牌号为空,可能无可用车位
 */
@Data
public class LegalParking implements Serializable {

    private Integer lid;
    private String carLicense;
    private Integer uid;
    private String pid;
    private Integer type;
    private Integer status;

}
