package team.lhc.cms.util;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 文
 * @description json的返回类
 * @param <Data>
 */
@Slf4j
@Getter
@Setter
public class JsonResult<Data> implements Serializable {

    /**
     * 错误码：
     * ** 通用 **
     * 101：成功
     * 102：失败
     *
     *
     * ** 停车场相关 **
     * 103：放行
     * 104：不放行
     *
     *
     * ** 登录相关 **
     * 201：用户未登录
     * 202：用户账号或密码错误
     * 203：用户登录成功
     * 204：用户无权访问
     * 205：用户登出成功
     * 206：此token为黑名单
     * 207：登录已失效
     *
     *
     * ** 异常相关 **
     * 系统异常:
     * 1001:运行时异常
     * 1002:空指针异常
     * 1003:类型转换异常
     * 1004:IO异常
     * 1005:未知方法异常
     * 1006:数组越界异常
     * 1007:400错误:Http消息不可读异常
     * 1008:400错误:类型不匹配异常
     * 1009:400错误:缺少Servlet请求参数异常
     * 1010:405错误
     * 1011:406错误
     * 1012:500错误
     * 1013:栈溢出
     * 1014:除数不能为0
     * 1015:404错误
     * 1016:其他系统级错误
     *
     * 自定义异常
     * 1040:token过期异常
     */

    private Integer code;
    private String msg;
    private Map<String,Object> data;

    public JsonResult(){

    };

    public JsonResult(int code, String msg, Map<String, Object> data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }


    public static String success() {
        return success(new HashMap(0));
    }
    public static String success(String msg){return success(101,msg);}
    public static String success(int code, String msg) {
        return  JSON.toJSONString(new JsonResult(code, msg, new HashMap(0)));
    }
    public static String success(Map<String, Object> data) {
        return JSON.toJSONString(new JsonResult(101, "成功", data));
    }
    public static String success(String msg,Map<String, Object> data){
        return JSON.toJSONString(new JsonResult(101, msg, data));
    }
    public static String success(int code,String msg,Map<String,Object> data){
        return JSON.toJSONString(new JsonResult(code, msg, data));
    }


    public static String failed() {
        return failed("解析失败");
    }
    public static String failed(String msg) {
        return failed(102, msg);
    }
    public static String failed(int code, String msg) {
        return  JSON.toJSONString(new JsonResult(code, msg, new HashMap(0)));
    }

}
