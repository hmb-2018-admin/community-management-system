package team.lhc.cms.util;

import lombok.Getter;
import team.lhc.cms.enums.ResultCode;

import java.io.Serializable;
import java.util.List;

/**
 * 自定义统一响应体
 */
@Getter
public class ResultVO<T> implements Serializable {

    // 状态码 默认101是成功
    private int code;

    // 响应信息 说明响应情况
    private String msg;

    // 响应的具体数据
    private T data;

    public ResultVO(int code,String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    public ResultVO(ResultCode resultCode, T data) {
        this.code = resultCode.getCode();
        this.msg = resultCode.getMsg();
        this.data = data;
    }
    public ResultVO(int code,String msg) {
        this.code = code;
        this.msg = msg;
    }


    public static ResultVO success(String msg){
        return new ResultVO(101,msg);
    }
    public static <T> ResultVO success(T data){
        return new ResultVO(ResultCode.SUCCESS,data);
    }

    public static ResultVO failed(String msg){
        return new ResultVO(102,msg);
    }

}
