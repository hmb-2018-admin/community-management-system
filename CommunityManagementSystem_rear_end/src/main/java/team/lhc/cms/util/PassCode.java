package team.lhc.cms.util;


import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

/**
 * @author 文
 * @description 通行码的工具类
 */
@Slf4j
public class PassCode {

    /**
     * 创建通行码
     * @return
     */
    public static String createPassCode(){
        return UUID.randomUUID().toString();
    }

}
