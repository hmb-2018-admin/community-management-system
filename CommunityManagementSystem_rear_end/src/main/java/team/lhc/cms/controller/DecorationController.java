package team.lhc.cms.controller;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import team.lhc.cms.components.security.utils.JwtTokenUtils;
import team.lhc.cms.entity.Decoration;
import team.lhc.cms.enums.ResultCode;
import team.lhc.cms.service.DecorationService;
import team.lhc.cms.util.ResultVO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "装修")
@RestController
@CrossOrigin
public class DecorationController {
    @Autowired
    private DecorationService decorationService;

    @ApiOperation("查询装修信息")
    @GetMapping("/management/decoration")
    public ResultVO getDecoration(Decoration decoration){
        return decorationService.getDecoration(decoration);
    }

    @ApiOperation("申请装修信息")
    @PostMapping("/proprietor/decoration")
    public ResultVO insertDecoration(MultipartFile[] pics, Decoration decoration, HttpServletRequest request){
        String token = request.getHeader(JwtTokenUtils.TOKEN_HEADER);
        return decorationService.insertDecoration(pics,decoration,token);
    }

    @ApiOperation("处理装修信息")
    @PutMapping("/management/decoration/{did}")
    public ResultVO putDecoration(@PathVariable String did){
        return decorationService.putDecoration(did);
    }

    @ApiOperation("删除装修信息")
    @DeleteMapping("/management/decoration/{did}")
    public ResultVO delDecration(@PathVariable String did){
            return decorationService.delDecoration(did);
    }

    @ApiOperation("分页查询装修信息")
    @GetMapping("/management/pageDecoration/{pageNum}/{pageSize}")
    public ResultVO pageDecoration(@PathVariable int pageNum, @PathVariable int pageSize){
        return ResultVO.success(decorationService.pageDecoration(pageNum,pageSize));
    }


}
