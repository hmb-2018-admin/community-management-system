package team.lhc.cms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import team.lhc.cms.entity.ParkingSpace;
import team.lhc.cms.service.ParkingService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * @author 文
 * @description 停车/车位视图层
 */
@Slf4j
@Api(tags = "车位")
@RestController
public class ParkingController {

    @Autowired
    ParkingService parkingServiceImpl;

    /**
     * 设置车牌号
     * @author 文
     * @param licenses
     * @param httpServletRequest
     * @return
     */
    @ApiOperation("设置车牌号")
    @PostMapping("/proprietor/license")
    public String setLicense(@RequestBody Map<String,Object> licenses, HttpServletRequest httpServletRequest){
        String token = httpServletRequest.getHeader("Authorization");
        log.info(licenses.toString());
        return parkingServiceImpl.setLicense(licenses,token);
    }

    /**
     * 设置(更新)某个车位的信息
     * @author 文
     * @param parkingSpace
     * @return
     */
    @ApiOperation("设置(更新)某个车位的信息")
    @PutMapping("/management/parkingSpace")
    public String setParkingSpaceInfo(ParkingSpace parkingSpace){
        return parkingServiceImpl.setParkingSpaceInfo(parkingSpace);
    }

    /**
     * 入场检查
     * @author 文
     * @param license
     * @return
     */
    @ApiOperation("入场检查")
    @GetMapping("/management/entry")
    public String entry(String license) throws ParseException {
        return parkingServiceImpl.entry(license);
    }

    /**
     * 出场计费
     * @author 文
     * @param license
     * @return
     */
    @ApiOperation("出场计费")
    @GetMapping("/management/departure")
    public String departure(String license) throws ParseException {
        return parkingServiceImpl.departure(license);
    }

    /**
     * 导入车位
     * @author 文
     * @param file
     * @return
     * @throws IOException
     */
    @ApiOperation("导入车位")
    @PostMapping("/management/importParkingSpace")
    public String perImportParkingSpace(@RequestParam("file") MultipartFile file) throws IOException {
        return parkingServiceImpl.importParkingSpace(file);
    }

    /**
     * 下载模板
     * @author 文
     * @param response
     * @throws IOException
     */
    @ApiOperation("下载模板")
    @GetMapping("/management/parkingSpaceTemplate")
    public void downloadTemplate(HttpServletResponse response) throws IOException {
        parkingServiceImpl.downloadTemplate(response);
    }

    /**
     * 获取用户可设置车位数和已存在车牌
     * @author 文
     * @param httpServletRequest
     * @return
     */
    @ApiOperation("获取用户可设置车位数和已存在车牌")
    @GetMapping("/proprietor/getLicense")
    public String getLicense(HttpServletRequest httpServletRequest){
        String token = httpServletRequest.getHeader("Authorization");
        return parkingServiceImpl.getLicense(token);
    }

    /**
     * 获取所有停车位信息
     * @author 文
     * @return
     */
    @ApiOperation("获取所有停车位信息")
    @GetMapping("/management/parkingSpaces")
    public String getList(){
        return parkingServiceImpl.getParkingSpaceList();
    }



}
