package team.lhc.cms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import team.lhc.cms.components.security.utils.JwtTokenUtils;
import team.lhc.cms.entity.Pager;
import team.lhc.cms.entity.Proprietor;
import team.lhc.cms.enums.ResultCode;
import team.lhc.cms.service.ProprietorService;
import team.lhc.cms.util.ResultVO;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业主的控制层
 */
@Api(tags = "业主")
@RestController
@RequestMapping("proprietor")
public class ProprietorController {

    @Autowired
    ProprietorService proprietorService;

    ResultVO resultVO = null;

    /**
     * 添加业主
     * @param proprietor
     * @return
     */
    @ApiOperation("添加业主")
    @PostMapping("insert")
    public ResultVO insertPro(@RequestBody Proprietor proprietor){
        int success = proprietorService.insertPro(proprietor);
        resultVO = new ResultVO(ResultCode.SUCCESS, success);
        return resultVO;
    }

    /**
     * 查询全部业主
     * @return
     */
    @ApiOperation("查询全部业主")
    @GetMapping("findAll")
    public ResultVO findAll(){
        List<Proprietor> proprietor = proprietorService.findAll();
        System.out.println(proprietor);
        resultVO = new ResultVO(ResultCode.SUCCESS, proprietor);
        return  resultVO;
    }

    /**
     * 通过id查询业主
     */
    @ApiOperation("通过id查询业主")
    @GetMapping("findById/{id}")
    public ResultVO findById(@PathVariable("id") int id){
        Proprietor proprietor = proprietorService.findById(id);
        if(proprietor == null){
            resultVO = new ResultVO(ResultCode.FAILED,proprietor);
        }else {
            resultVO = new ResultVO(ResultCode.SUCCESS,proprietor);
        }
        return resultVO;
    }

    /**
     * 通过业主名字查询业主
     */
    @ApiOperation("通过业主名字查询业主")
    @GetMapping("findByName/{name}")
    public ResultVO findByName(@PathVariable("name") String name){
        Proprietor proprietor = proprietorService.findByName(name);
        if(proprietor == null){
            resultVO = new ResultVO(ResultCode.FAILED,proprietor);
        }else {
            resultVO = new ResultVO(ResultCode.SUCCESS,proprietor);
        }
        resultVO = new ResultVO(ResultCode.SUCCESS,proprietor);
        return resultVO;
    }

    /**
     * 通过房号查询业主
     */
    @ApiOperation("通过房号查询业主")
    @GetMapping("findByRoomId/{roomId}")
    public ResultVO findByRoomId(@PathVariable("roomId") int roomId){
        Proprietor proprietor = proprietorService.findByRoomId(roomId);
        if(proprietor == null){
            resultVO = new ResultVO(ResultCode.FAILED,proprietor);
        }else {
            resultVO = new ResultVO(ResultCode.SUCCESS,proprietor);
        }
        resultVO = new ResultVO(ResultCode.SUCCESS,proprietor);
        return resultVO;
    }

    /**
     * 分页
     */
    @ApiOperation("分页")
    @GetMapping("findByPage/{page}/{size}")
    public ResultVO findByPage(@PathVariable("page") int page,@PathVariable("size") int size){
        Map<String, Object> params = new HashMap<>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        Pager<Proprietor> pager = new Pager<>();
        List<Proprietor> list = proprietorService.findByPage(params);
        pager.setRows(list);
        pager.setTotal(proprietorService.totalPro());
        resultVO = new ResultVO(ResultCode.SUCCESS,pager);
        return resultVO;
    }

    /**
     * 通过id删除业主信息
     */
    @ApiOperation("通过id删除业主信息")
    @DeleteMapping("delete/{id}")
    public ResultVO deleteProById(@PathVariable("id") int id){
        int success = proprietorService.deleteProById(id);
        resultVO = new ResultVO(ResultCode.SUCCESS,success);
        return resultVO;
    }

    /**
     * 通过业主名字删除业主信息
     */
    @ApiOperation("通过业主名字删除业主信息")
    @DeleteMapping("deleteByName/{name}")
    public ResultVO deleteProById(@PathVariable("name") String name){
        int success = proprietorService.deleteProByName(name);
        resultVO = new ResultVO(ResultCode.SUCCESS,success);
        return resultVO;
    }

    /**
     * 通过房号删除业主信息
     */
    @ApiOperation("通过房号删除业主信息")
    @DeleteMapping("deleteByRoomId/{roomId}")
    public ResultVO deleteProByRoomId(@PathVariable("roomId") int roomId){
        int success = proprietorService.deleteProByRoomId(roomId);
        resultVO = new ResultVO(ResultCode.SUCCESS,success);
        return resultVO;
    }

    /**
     * 更新业主信息
     */
    @ApiOperation("更新业主信息")
    @PostMapping("update")
    public ResultVO updatePro(@RequestBody Proprietor proprietor){
        int success = proprietorService.updatePro(proprietor);
        resultVO = new ResultVO(ResultCode.SUCCESS,success);
        return resultVO;
    }

    /**
     * 更新业主手机号
     * @author 文
     * @param tel
     * @param request
     * @return
     */
    @ApiOperation("更新业主手机号")
    @PostMapping("/updateTel")
    public String updateTel(String tel, HttpServletRequest request){
        String token = request.getHeader(JwtTokenUtils.TOKEN_HEADER);
        return proprietorService.updateTel(tel,token);
    }

    /**
     * 获取业主信息
     * @author 文
     * @param request
     * @return
     */
    @ApiOperation("获取业主信息")
    @GetMapping("/proprietor")
    public String getProprietorInfo(HttpServletRequest request){
        String token = request.getHeader(JwtTokenUtils.TOKEN_HEADER);
        return proprietorService.getInfo(token);
    }


}
