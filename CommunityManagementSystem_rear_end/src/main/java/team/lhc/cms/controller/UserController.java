package team.lhc.cms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import team.lhc.cms.components.security.utils.JwtTokenUtils;
import team.lhc.cms.dto.UserDTO;
import team.lhc.cms.service.UserService;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 文
 * @description 用户的视图层
 */
@Api(tags = "用户")
@RestController
public class UserController {

    @Autowired
    UserService userService;

    /**
     * 获取用户信息列表
     * @author 文
     * @return
     */
    @ApiOperation("获取用户信息列表")
    @GetMapping("/management/users")
    public String getList(){
        return userService.getList();
    }

    /**
     * 更新用户信息(只能更新用户名和上锁状态)
     * @author 文
     * @param userDTO
     * @return
     */
    @ApiOperation("更新用户信息(只能更新用户名和上锁状态)")
    @PutMapping("/management/user")
    public String update(UserDTO userDTO){
        return userService.update(userDTO);
    }

    /**
     * 获取用户信息(因前端封装了,所以才要这个接口)
     * @author 文
     * @author
     * @param request
     * @return
     */
    @ApiOperation("获取用户信息")
    @GetMapping("/public/info")
    public String getInfo(HttpServletRequest request){
        String token = request.getHeader(JwtTokenUtils.TOKEN_HEADER);
        return userService.getInfo(token);
    }
}
