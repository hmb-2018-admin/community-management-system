package team.lhc.cms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import team.lhc.cms.service.PayService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 文
 * @description 缴费系统视图层
 */
@Api(tags = "缴费")
@RestController
public class PayController {
    @Autowired
    PayService payServiceImpl;

    /**
     * 下载缴费清单模板
     * @author 文
     * @param httpServletResponse
     * @throws IOException
     */
    @ApiOperation("下载缴费清单模板")
    @GetMapping("/management/paymentListTemplate")
    public void downloadPaymentListTemplate(HttpServletResponse httpServletResponse) throws IOException {
        payServiceImpl.downloadTemplate(httpServletResponse);
    }

    /**
     * 导入缴费记录
     * @author 文
     * @param file
     * @return
     * @throws IOException
     */
    @ApiOperation("导入缴费记录")
    @PostMapping("/management/importPaymentList")
    public String importPaymentList(@RequestParam("file") MultipartFile file) throws IOException {
        return payServiceImpl.importPaymentList(file);
    }

    /**
     * 查看自己的账单
     * @author 文
     * @param httpServletRequest
     * @return
     */
    @ApiOperation("查看自己的账单")
    @GetMapping("/proprietor/payments")
    public String lookOverPaymentList(HttpServletRequest httpServletRequest){
        String token = httpServletRequest.getHeader("Authorization");
        return payServiceImpl.lookOverPaymentList(token);
    }

    /**
     * 查看某一条账单
     * @author 文
     * @param costId
     * @return
     */
    @ApiOperation("查看某一条账单")
    @GetMapping("/public/payment/{costId}")
    public String lookOverPayment(@PathVariable("costId") Integer costId){
        return payServiceImpl.lookOverPayment(costId);
    }

    /**
     * 根据条件(房号或日期)查询账单
     * @author 文
     * @param roomId
     * @param beginTime
     * @param endTime
     * @param isPay
     * @return
     */
    @ApiOperation("根据条件(房号或日期)查询账单")
    @GetMapping("/management/payments")
    public String lookOverPaymentListByCondition(String roomId, String beginTime, String endTime, String isPay){
        return payServiceImpl.searchPayment(roomId,beginTime,endTime,isPay);
    }
}
