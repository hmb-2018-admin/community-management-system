package team.lhc.cms.controller;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import team.lhc.cms.components.security.utils.JwtTokenUtils;
import team.lhc.cms.entity.Decoration;
import team.lhc.cms.entity.Repair;
import team.lhc.cms.enums.ResultCode;
import team.lhc.cms.service.RepairService;
import team.lhc.cms.util.JsonResult;
import team.lhc.cms.util.ResultVO;

import javax.management.Query;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "报修")
@RestController
@CrossOrigin
public class RepairController {
    @Autowired
    private RepairService repairService;

    @ApiOperation("提交报修信息")
    @PostMapping("/public/repair")
    public ResultVO addRepair(MultipartFile[] pics, Repair repair, HttpServletRequest request){
        String token = request.getHeader(JwtTokenUtils.TOKEN_HEADER);
        return repairService.addRepair(pics,repair,token);
    }

    @ApiOperation("根据uid查找报修信息")
    @GetMapping("/public/repair")
    public ResultVO findRepairByUid(HttpServletRequest request){
        String token = request.getHeader(JwtTokenUtils.TOKEN_HEADER);
        return repairService.findRepairByUid(token);
    }

    @ApiOperation("修改报修状态为已完成")
    @PutMapping("/management/repair/{repairId}")
    public ResultVO updateSolve(@PathVariable long repairId){
        return repairService.updateSolve(repairId);
    }

    @ApiOperation("删除报修信息")
    @DeleteMapping("/management/repair/{repairId}")
    public ResultVO delRepair(@PathVariable long repairId){
        return repairService.delRepair(repairId);
    }

    @ApiOperation("查询所有报修信息")
    @GetMapping("/management/repairAll")
    public ResultVO findRepair(){
        List<Repair> repairs = repairService.findRepair();
        return ResultVO.success(repairs);
    }

    @ApiOperation("分页查询报修信息")
    @GetMapping("/management/pageRepair/{pageNum}/{pageSize}")
    public ResultVO pageRepair(@PathVariable int pageNum,@PathVariable int pageSize){
        return ResultVO.success(repairService.pageRepair(pageNum,pageSize));
    }

    @ApiOperation("查询筛选报修信息")
    @GetMapping("/management/repair")
    public ResultVO getRepair(Repair repair){
        return repairService.getRepair(repair);
    }

}
