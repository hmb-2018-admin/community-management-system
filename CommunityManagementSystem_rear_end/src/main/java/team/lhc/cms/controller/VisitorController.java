package team.lhc.cms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import team.lhc.cms.components.security.utils.JwtTokenUtils;
import team.lhc.cms.entity.Visitor;
import team.lhc.cms.service.VisitorService;
import team.lhc.cms.util.JsonResult;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;
import java.util.Map;


/**
 * @author 文
 * @description 访客的视图层
 */
@Api(tags = "访客")
@RestController
public class VisitorController {

    @Autowired
    VisitorService visitorServiceImpl;

    /**
     * 申请访客通行码
     * @author 文
     * @return
     */
    @ApiOperation("申请访客通行码")
    @PostMapping("/proprietor/visitor")
    public String applyPassCode(Visitor visitor,HttpServletRequest request) throws ParseException {
        String token = request.getHeader("Authorization");
        Map<String,Object> apply = visitorServiceImpl.apply(visitor,token);
        return "0".equals(apply.get("passCode"))?JsonResult.failed("创建失败,请检查填写是否合法或稍后再申请"):JsonResult.success("创建成功,请保存通行码,凭通行码和电话在当天可进入小区。",apply);
    }

    /**
     * 验证通行码
     * @author 文
     * @param passCode 通行码
     * @param tel 联系电话
     * @return
     */
    @ApiOperation("验证通行码")
    @GetMapping("/guard/verification")
    public String verificationCode(String passCode,String tel){
        return visitorServiceImpl.verification(passCode,tel);
    }


    /**
     * 获取自己的申请历史记录
     * @author 文
     * @param httpServletRequest
     * @return
     */
    @ApiOperation("获取自己的申请历史记录")
    @GetMapping("/proprietor/visitors")
    public List<Visitor> getRecordList(HttpServletRequest httpServletRequest){
        String token = httpServletRequest.getHeader("Authorization");
        return visitorServiceImpl.getRecordList(token);
    }

    /**
     * 查看某条记录
     * @author 文
     * @param visitId
     * @param httpServletRequest
     * @return
     */
    @ApiOperation("查看某条记录")
    @GetMapping("/proprietor/visitor/{visitId}")
    public String getRecord(@PathVariable("visitId") String visitId,HttpServletRequest httpServletRequest){
        String token = httpServletRequest.getHeader(JwtTokenUtils.TOKEN_HEADER);
        return visitorServiceImpl.getRecord(token,visitId);
    }
}
