package team.lhc.cms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import team.lhc.cms.components.security.utils.JwtTokenUtils;
import team.lhc.cms.service.DoorService;
import javax.servlet.http.HttpServletRequest;

/**
 * @author 文
 * @description 门禁的视图层
 */
@Api(tags = "门禁")
@RestController
public class DoorController {

    @Autowired
    DoorService doorService;

    /**
     * 开门
     * @author 文
     * @param doorName
     * @param request
     * @return
     */
    @ApiOperation("开门")
    @GetMapping("/public/open")
    public String openDoor(String doorName, HttpServletRequest request){
        String token = request.getHeader(JwtTokenUtils.TOKEN_HEADER);
        return doorService.openDoor(doorName,token);
    }
}
