package team.lhc.cms.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Management implements Serializable{
    private Integer mid;
    private String manName;
    private String uid;
}
