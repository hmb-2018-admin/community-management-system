package team.lhc.cms.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 文
 * @description 车牌实体类
 */
@Data
public class License implements Serializable {

    private Integer lid;
    private String carLicense;
    private Integer uid;


}
