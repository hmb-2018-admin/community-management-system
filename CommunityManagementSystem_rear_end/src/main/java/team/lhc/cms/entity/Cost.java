package team.lhc.cms.entity;

import lombok.Data;

import java.io.Serializable;


@Data
public class Cost implements Serializable{
    private Integer costId;
    private String roomId;
    private String carFee;
    private String proFee;
    private String elFee;
    private String elDegree;
    private String waterFee;
    private String waterTonnes;
    private String gasFee;
    private String gasVol;
    private String total;
    private String cTime;
    private Integer status;


}

