package team.lhc.cms.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Proprietor  implements Serializable{
    private Integer pid;
    private String roomId;
    private String propName;
    private String idCard;
    private String tel;
    private Integer carId;
    private String carType;
    private Integer uid;

}
