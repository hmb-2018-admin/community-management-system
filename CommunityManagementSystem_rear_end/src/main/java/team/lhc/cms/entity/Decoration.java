package team.lhc.cms.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
public class Decoration {
    private Integer did;
    private String description;
    private Integer solve;
    private Integer uid;
    private String propName;
    private String tel;

    private List<Pictures> urls;

}
