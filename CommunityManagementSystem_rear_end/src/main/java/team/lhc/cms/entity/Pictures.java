package team.lhc.cms.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class Pictures {
    private Integer pid;
    private String url;
    private Integer did;
}
