package team.lhc.cms.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 文
 * @description 停车记录实体类
 */
@Data
public class ParkingRecord implements Serializable {

    private Integer rid;
    private String license;
    private String pid;
    private String entryTime;
    private String departureTime;
    private String fee;

    public ParkingRecord(String license, String pid, String entryTime) {
        this.license = license;
        this.pid = pid;
        this.entryTime = entryTime;
    }
}
