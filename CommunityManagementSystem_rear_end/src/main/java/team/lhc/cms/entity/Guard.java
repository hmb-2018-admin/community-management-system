package team.lhc.cms.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Guard implements Serializable {
    private Integer guardId;
    private String guardName;
    private String guardPeriod;
    private String idCard;
    private String tel;
    private Integer uid;
}
