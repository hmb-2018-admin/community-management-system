package team.lhc.cms.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Door implements Serializable{
    private Integer doorId;
    private Integer number;
    private String applySituation;
    private String applyNumber;
    private String roomId;

}
