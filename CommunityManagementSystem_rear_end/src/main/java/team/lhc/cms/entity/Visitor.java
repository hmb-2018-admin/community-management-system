package team.lhc.cms.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 文
 * @description 访客实体类
 */
@Data
public class Visitor implements Serializable {
    private Integer visitId;
    private Integer visitorNumber;
    private String reason;
    private String visitorName;
    private String visitorTel;
    private String roomId;
    private String visitTime;
    private Integer days;
    private String tempLicense;
    private String uuid;
    private Integer uid;
}
