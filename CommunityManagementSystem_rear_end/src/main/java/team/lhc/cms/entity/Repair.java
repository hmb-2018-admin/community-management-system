package team.lhc.cms.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.security.core.parameters.P;

import java.io.Serializable;
import java.util.List;

@Data
public class Repair implements Serializable {
    private Integer repairID;
    private String location;
    private String repairSituation;
    private Integer solve;
    private String time;
    private Integer uid;
    private String propName;
    private String tel;

    private List<Pictures> urls;

}
