package team.lhc.cms.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 文
 * @description 车位实体类
 */
@Data
public class ParkingSpace implements Serializable {

    private String pid;
    private Integer type;
    private Integer status;
    private Integer uid;

}
