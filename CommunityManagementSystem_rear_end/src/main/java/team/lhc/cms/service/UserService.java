package team.lhc.cms.service;

import team.lhc.cms.dto.UserDTO;

import java.util.Map;

/**
 * @author 文
 * @description 用户业务层接口类
 */
public interface UserService {


    /**
     * 用户解绑
     * @author 文
     * @param role
     * @param token
     * @return
     */
    String untie(String role, String token);

    /**
     * 注册用户
     * @author 文
     * @param registerUser
     * @return
     */
    String register(Map<String, String> registerUser);

    /**
     * 获取用户信息
     * @author 文
     * @param token
     * @return
     */
    String getInfo(String token);

    /**
     * 获取全部用户
     * @author 文
     * @return
     */
    String getList();

    /**
     * 更新用户信息
     * @author 文
     * @param userDTO
     * @return
     */
    String update(UserDTO userDTO);
}
