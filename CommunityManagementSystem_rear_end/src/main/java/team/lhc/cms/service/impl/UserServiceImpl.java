package team.lhc.cms.service.impl;

import com.qiniu.util.Json;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import team.lhc.cms.dto.UserDTO;
import team.lhc.cms.entity.User;
import team.lhc.cms.mapper.GuardMapper;
import team.lhc.cms.mapper.ProprietorMapper;
import team.lhc.cms.mapper.UserMapper;
import team.lhc.cms.service.UserService;
import team.lhc.cms.util.JsonResult;
import team.lhc.cms.util.RedisUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 文
 * @description 用户业务层实现类
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProprietorMapper proprietorMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private GuardMapper guardMapper;

    /**
     * 用户解绑
     * @author 文
     * @param role
     * @param token
     * @return
     */
    @Override
    public String untie(String role, String token) {
        Map<String,Object> data = (Map<String, Object>) redisUtil.hget(token);
        Integer uid = (Integer) data.get("uid");
        int num = 0;
        switch (role){
            case "0":
                num += proprietorMapper.setUidNullByUid(uid);
                num += userMapper.setRoleNullByUid(uid);
                return num == 2 ? JsonResult.success("解绑成功") : JsonResult.failed("解绑失败,请重试");
            case "1":
                num = guardMapper.setUidNullByUid(uid);
                num += userMapper.setRoleNullByUid(uid);
                return num == 2 ? JsonResult.success("解绑成功") : JsonResult.failed("解绑失败,请重试");
            default:
                return JsonResult.failed("改角色暂不支持解绑");
        }
    }

    /**
     * 注册用户
     * @author 文
     * @param registerUser
     * @return
     */
    @Override
    public String register(Map<String, String> registerUser) {
        User user = new User();
        user.setUsername(registerUser.get("username"));
        user.setPassword(bCryptPasswordEncoder.encode(registerUser.get("password")));
        String role = null;
        switch (registerUser.get("role")){
            case "0":role = "ROLE_PROPRIETOR";user.setIsLocked(0);break;
            case "1":role = "ROLE_GUARD";user.setIsLocked(0);break;
            case "2":role = "ROLE_MANAGEMENT";user.setIsLocked(0);break;
            default:return JsonResult.failed("权限编号错误");
        }
        user.setRole(role);
        userMapper.save(user);
        Map<String,Object> map = new HashMap<>();
        map.put("user",user);
        if (user.getId() != null){
            return JsonResult.success("创建成功",map);
        }else {
            return JsonResult.failed("创建出错");
        }

    }

    /**
     * 获取用户信息
     * @author 文
     * @param token
     * @return
     */
    @Override
    public String getInfo(String token) {
        Map<String,Object> data = (Map<String, Object>) redisUtil.hget(token);
        User user = userMapper.findByUid((Integer) data.get("uid"));
        data.remove("uid");
        data.put("username",user.getUsername());
        data.put("token",token);
        data.put("role",user.getRole());
        return JsonResult.success(data);
    }

    /**
     * 获取用户信息列表
     * @author 文
     * @return
     */
    @Override
    public String getList() {
        List<UserDTO> list = userMapper.getList();
        Map<String,Object> map = new HashMap<>();
        map.put("users",list);
        return JsonResult.success(map);
    }

    /**
     * 更新用户信息
     * @author 文
     * @param userDTO
     * @return
     */
    @Override
    public String update(UserDTO userDTO) {
        Integer update = userMapper.update(userDTO);
        if (update == 0){
            return JsonResult.failed("修改失败,请重试");
        }
        return JsonResult.success("修改成功");
    }
}
