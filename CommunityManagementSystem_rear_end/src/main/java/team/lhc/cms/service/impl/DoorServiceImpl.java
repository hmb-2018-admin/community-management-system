package team.lhc.cms.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team.lhc.cms.service.DoorService;
import team.lhc.cms.util.JsonResult;
import team.lhc.cms.util.RedisUtil;

import java.util.Map;

/**
 * @author 文
 * @description 门禁服务层实现类
 */
@Service
@Slf4j
public class DoorServiceImpl implements DoorService {

    @Autowired
    RedisUtil redisUtil;

    /**
     * 开门
     * @author 文
     * @param doorName
     * @param token
     * @return
     */
    @Override
    public String openDoor(String doorName, String token) {
        Map<String,Object> data = (Map<String, Object>) redisUtil.hget(token);
        Integer uid = (Integer) data.get("uid");
        //输出日志作分析([open][1][开门])
        log.info("[open][" + uid + "][" + doorName + "]");
        return JsonResult.success("开门成功");
    }
}
