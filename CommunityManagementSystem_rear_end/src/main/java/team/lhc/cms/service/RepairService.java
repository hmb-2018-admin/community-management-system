package team.lhc.cms.service;

import org.springframework.web.multipart.MultipartFile;
import team.lhc.cms.entity.Repair;
import team.lhc.cms.util.ResultVO;

import java.util.List;

public interface RepairService {
    ResultVO addRepair(MultipartFile[] pics, Repair repair,String token);
    ResultVO findRepairByUid(String token);
    ResultVO updateSolve(Long repairId);
    ResultVO delRepair(Long repairId);
    ResultVO findRepairByRepairId(Long repairId);
    List<Repair> findRepair();
    ResultVO pageRepair(int pageNum, int pageSize);
    ResultVO getRepair(Repair repair);
}
