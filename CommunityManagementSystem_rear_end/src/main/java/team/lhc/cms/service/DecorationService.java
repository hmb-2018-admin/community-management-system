package team.lhc.cms.service;

import com.github.pagehelper.Page;
import org.springframework.web.multipart.MultipartFile;
import team.lhc.cms.entity.Decoration;
import team.lhc.cms.util.ResultVO;

public interface DecorationService {
    ResultVO getDecoration(Decoration decoration);
    ResultVO insertDecoration(MultipartFile[] pics, Decoration decoration,String token);
    ResultVO putDecoration(String did);
    ResultVO delDecoration(String did);
    Page<Decoration> pageDecoration(int pageNum, int pageSize);
}
