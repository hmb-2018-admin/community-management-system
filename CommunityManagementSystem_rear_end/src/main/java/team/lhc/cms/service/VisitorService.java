package team.lhc.cms.service;

import team.lhc.cms.entity.Visitor;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * @author 文
 * @description 访客的service层接口
 */
public interface VisitorService {
    /**
     * 申请通行码
     * @author 文
     * @param visitor
     * @return
     * @throws ParseException
     */
    Map<String,Object> apply(Visitor visitor,String token) throws ParseException;

    /**
     * 验证通行码
     * @author 文
     * @param passCode
     * @param tel
     * @return
     */
    String verification(String passCode, String tel);

    /**
     * 获取申请的历史记录
     * @author 文
     * @param token
     * @return
     */
    List<Visitor> getRecordList(String token);

    /**
     * 获取某条申请记录
     * @author 文
     * @param token
     * @param visitId
     * @return
     */
    String getRecord(String token, String visitId);
}
