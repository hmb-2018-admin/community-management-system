package team.lhc.cms.service;

import org.springframework.web.multipart.MultipartFile;
import team.lhc.cms.entity.ParkingSpace;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * @author 文
 * @description 停车/车位服务类
 */
public interface ParkingService {

    /**
     * 设置车牌号
     * @author 文
     * @param list
     * @param token
     * @return
     */
    String setLicense(Map<String,Object> list, String token);

    /**
     * 设置停车信息
     * @author 文
     * @param parkingSpace
     * @return
     */
    String setParkingSpaceInfo(ParkingSpace parkingSpace);

    /**
     * 车辆入场
     * @author 文
     * @param license
     * @return
     * @throws ParseException
     */
    String entry(String license) throws ParseException;

    /**
     * 车辆出场
     * @author 文
     * @param license
     * @return
     * @throws ParseException
     */
    String departure(String license) throws ParseException;

    /**
     * 导入车位
     * @author 文
     * @param file
     * @return
     */
    String importParkingSpace(MultipartFile file) throws IOException;

    /**
     * 下载导入车位模板
     * @author 文
     * @param response
     */
    void downloadTemplate(HttpServletResponse response) throws IOException;

    /**
     * 获取用户可设置车位数和已存在车牌
     * @author 文
     * @param token
     * @return
     */
    String getLicense(String token);

    /**
     * 获取停车位列表
     * @author 文
     * @return
     */
    String getParkingSpaceList();
}
