package team.lhc.cms.service;

import team.lhc.cms.entity.Proprietor;
import java.util.List;
import java.util.Map;

/**
 * Service接口
 */
public interface ProprietorService{

    List<Proprietor> findAll();
    Proprietor findById(int pid);
    Proprietor findByName(String name);
    Proprietor findByRoomId(int roomId);
    List<Proprietor> findByPage(Map<String, Object> params);
    int totalPro();
    int insertPro(Proprietor proprietor);
    int deleteProById(int pid);
    int deleteProByName(String name);
    int deleteProByRoomId(int roomId);
    int updatePro(Proprietor proprietor);

    /**
     * 业主更新手机号
     * @author 文
     * @param tel
     * @param token
     * @return
     */
    String updateTel(String tel, String token);

    /**
     * 获取业主信息
     * @author 文
     * @param token
     * @return
     */
    String getInfo(String token);
}
