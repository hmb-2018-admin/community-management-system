package team.lhc.cms.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 文
 * @description 缴费系统服务层接口
 */
public interface PayService {


    /**
     * 下载缴费清单模板
     * @author 文
     * @param httpServletResponse
     * @throws IOException
     */
    void downloadTemplate(HttpServletResponse httpServletResponse) throws IOException;

    /**
     * 导入缴费清单
     * @author 文
     * @param file
     * @return
     * @throws IOException
     */
    String importPaymentList(MultipartFile file) throws IOException;

    /**
     * 业主查看自己的账单
     * @author 文
     * @param token
     * @return
     */
    String lookOverPaymentList(String token);

    /**
     * 查看某一条账单
     * @author 文
     * @param costId
     * @return
     */
    String lookOverPayment(Integer costId);

    /**
     * 根据房号查看账单
     * @author 文
     * @param roomId
     * @return
     */
    String lookOverPaymentListByRoomId(String roomId);

    /**
     * 根据条件(房号或日期)查询账单
     * @author 文
     * @param roomId
     * @param beginTime
     * @param endTime
     * @param isPay
     * @return
     */
    String searchPayment(String roomId, String beginTime, String endTime, String isPay);
}
