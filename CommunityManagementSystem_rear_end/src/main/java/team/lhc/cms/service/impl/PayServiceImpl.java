package team.lhc.cms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import team.lhc.cms.entity.Cost;
import team.lhc.cms.entity.Proprietor;
import team.lhc.cms.mapper.CostMapper;
import team.lhc.cms.mapper.ProprietorMapper;
import team.lhc.cms.service.PayService;
import team.lhc.cms.util.ExcelUtil;
import team.lhc.cms.util.JsonResult;
import team.lhc.cms.util.RedisUtil;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 文
 * @description 缴费系统服务层实现类
 */
@Service
public class PayServiceImpl implements PayService {
    @Autowired
    CostMapper costMapper;

    @Autowired
    ProprietorMapper proprietorMapper;

    @Autowired
    RedisUtil redisUtil;


    /**
     * 下载缴费清单模板
     * @author 文
     * @param httpServletResponse
     * @throws IOException
     */
    @Override
    public void downloadTemplate(HttpServletResponse httpServletResponse) throws IOException {
        List<List<String>> lists = new ArrayList<>();
        List<String> list = null;
        List<String> rooms = proprietorMapper.getAllRoom();
        for (int i = 0; i < rooms.size() + 1; i++) {
            list = new ArrayList<>();
            if (i == 0){
                list.add("房号");
                list.add("车位管理费");
                list.add("物管费");
                list.add("电费");
                list.add("上月用电度数");
                list.add("水费");
                list.add("上月用水吨数");
                list.add("天然气费");
                list.add("上月用气立方数");
                list.add("账单年月(格式:xxxx-xx)");
            }else {
                list.add(rooms.get(i-1));
            }
            lists.add(list);
        }
        ExcelUtil.exportExcel(httpServletResponse,lists,"缴费表","缴费清单导入模板",25);
    }

    /**
     * 导入缴费清单
     * @author 文
     * @param file
     * @return
     * @throws IOException
     */
    @Override
    public String importPaymentList(MultipartFile file) throws IOException {
        List<List<String>> lists = ExcelUtil.importExcel(file);
        List<Cost> costs = new ArrayList<>();
        Cost cost = null;
        for (int i = 0; i < lists.size(); i++) {
            cost = new Cost();
            cost.setRoomId(lists.get(i).get(0));
            cost.setCarFee(lists.get(i).get(1)==null?"0":lists.get(i).get(1));
            cost.setProFee(lists.get(i).get(2)==null?"0":lists.get(i).get(2));
            cost.setElFee(lists.get(i).get(3)==null?"0":lists.get(i).get(3));
            cost.setElDegree(lists.get(i).get(4)==null?"0":lists.get(i).get(4));
            cost.setWaterFee(lists.get(i).get(5)==null?"0":lists.get(i).get(5));
            cost.setWaterTonnes(lists.get(i).get(6)==null?"0":lists.get(i).get(6));
            cost.setGasFee(lists.get(i).get(7)==null?"0":lists.get(i).get(7));
            cost.setGasVol(lists.get(i).get(8)==null?"0":lists.get(i).get(8));
            //计算总额
            double total = Double.parseDouble(cost.getCarFee()) + Double.parseDouble(cost.getElFee())
                    + Double.parseDouble(cost.getGasFee()) + Double.parseDouble(cost.getProFee()) + Double.parseDouble(cost.getWaterFee());
            cost.setTotal(String.valueOf(total));
            cost.setCTime(lists.get(i).get(9));
            costs.add(cost);
        }
        int save = costMapper.batSave(costs);
        return JsonResult.success("成功导入" + save + "条缴费记录");
    }

    /**
     * 业主查看自己的账单
     * @author 文
     * @param token
     * @return
     */
    @Override
    public String lookOverPaymentList(String token) {
        Map<String,Object> data = (Map<String, Object>) redisUtil.hget(token);
        Proprietor proprietor = proprietorMapper.findByUid((Integer) data.get("uid"));
        return lookOverPaymentListByRoomId(proprietor.getRoomId());
    }

    /**
     * 查看某一条账单
     * @author 文
     * @param costId
     * @return
     */
    @Override
    public String lookOverPayment(Integer costId) {
        Cost cost = costMapper.fineByCostId(costId);
        Map<String,Object> map = new HashMap<>();
        map.put("data",cost);
        return JsonResult.success(map);
    }

    /**
     * 根据房号查看账单
     * @author 文
     * @param roomId
     * @return
     */
    @Override
    public String lookOverPaymentListByRoomId(String roomId) {
        List<Cost> list = costMapper.getListByRoomId(roomId);
        Map<String,Object> map = new HashMap<>();
        map.put("data",list);
        return JsonResult.success(map);
    }

    /**
     * 根据条件(房号或日期)查询账单
     * @author 文
     * @param roomId
     * @param beginTime
     * @param endTime
     * @return
     */
    @Override
    public String searchPayment(String roomId, String beginTime, String endTime, String isPay) {
        List<Cost> costs = costMapper.findByCondition(roomId,beginTime,endTime,isPay);
        Map<String,Object> map = new HashMap<>();
        map.put("costs",costs);
        return JsonResult.success(map);
    }
}
