package team.lhc.cms.service;

/**
 * @author 文
 * @description 门禁服务层接口类
 */
public interface DoorService {

    /**
     * 开门
     * @author 文
     * @param doorName
     * @param token
     * @return
     */
    String openDoor(String doorName, String token);
}
