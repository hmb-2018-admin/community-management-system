package team.lhc.cms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team.lhc.cms.entity.Proprietor;
import team.lhc.cms.mapper.ProprietorMapper;
import team.lhc.cms.service.ProprietorService;
import team.lhc.cms.util.JsonResult;
import team.lhc.cms.util.RedisUtil;

import java.util.List;
import java.util.Map;

/**
 * 实现Service接口，实现功能
 */
@Service
public class ProprietorServiceImpl implements ProprietorService {

    @Autowired
    ProprietorMapper proprietorMapper;

    @Autowired
    RedisUtil redisUtil;

    @Override
    public List<Proprietor> findAll() {
        return proprietorMapper.findAll();
    }

    @Override
    public Proprietor findById(int pid) {
        return proprietorMapper.findById(pid);
    }

    @Override
    public Proprietor findByName(String name) {
        return proprietorMapper.findByName(name);
    }

    @Override
    public Proprietor findByRoomId(int roomId) {
        return proprietorMapper.findByRoomId(roomId);
    }

    @Override
    public List<Proprietor> findByPage(Map<String, Object> params) {
        return proprietorMapper.findByPage(params);
    }


    @Override
    public int totalPro() {
        return proprietorMapper.totalPro();
    }

    @Override
    public int insertPro(Proprietor proprietor) {
        return proprietorMapper.insertPro(proprietor);
    }

    @Override
    public int deleteProById(int pid) {
        return proprietorMapper.deleteProById(pid);
    }

    @Override
    public int deleteProByName(String name) {
        return proprietorMapper.deleteProByName(name);
    }

    @Override
    public int deleteProByRoomId(int roomId) {
        return proprietorMapper.deleteProByRoomId(roomId);
    }

    @Override
    public int updatePro(Proprietor proprietor) {
        return proprietorMapper.updatePro(proprietor);
    }

    /**
     * 更新手机号
     * @author 文
     * @param tel
     * @param token
     * @return
     */
    @Override
    public String updateTel(String tel, String token) {
        Map<String,Object> data = (Map<String, Object>) redisUtil.hget(token);
        Integer uid = (Integer) data.get("uid");
        int flag = proprietorMapper.updateTelByUid(tel,uid);
        return flag == 0 ? JsonResult.failed("更换手机号失败,请重试") : JsonResult.success("更换手机号成功");
    }

    /**
     * 获取业主信息
     * @author 文
     * @param token
     * @return
     */
    @Override
    public String getInfo(String token) {
        Map<String,Object> data = (Map<String, Object>) redisUtil.hget(token);
        Integer uid = (Integer) data.get("uid");
        Proprietor proprietor = proprietorMapper.findByUid(uid);
        if (proprietor == null){
            return JsonResult.failed("获取失败,请稍后重试");
        }
        data.remove(uid);
        data.put("roomId",proprietor.getRoomId());
        data.put("name",proprietor.getPropName());
        data.put("tel",proprietor.getTel());
        return JsonResult.success(data);
    }


}
