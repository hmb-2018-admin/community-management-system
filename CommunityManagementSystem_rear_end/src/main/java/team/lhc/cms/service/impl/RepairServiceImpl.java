package team.lhc.cms.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import team.lhc.cms.config.QiniuUpload;
import team.lhc.cms.entity.Pictures;
import team.lhc.cms.entity.Repair;
import team.lhc.cms.mapper.PicturesMapper;
import team.lhc.cms.mapper.RepairMapper;
import team.lhc.cms.service.RepairService;
import team.lhc.cms.util.RedisUtil;
import team.lhc.cms.util.ResultVO;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class RepairServiceImpl implements RepairService {

    @Autowired
    private RepairMapper repairMapper;
    @Autowired
    private PicturesMapper picturesMapper;
    @Autowired
    private RedisUtil redisUtil;
    @Override
    public ResultVO addRepair(MultipartFile[] pics, Repair repair,String token) {
        try {
            Map<String,Object> hget = (Map<String,Object>)redisUtil.hget(token);
            Integer uid = (Integer) hget.get("uid");
            repair.setSolve(0);
            repair.setUid(uid);
            repairMapper.addRepair(repair);
            for(MultipartFile file:pics){
                String filename = file.getOriginalFilename();
                String suffix  = filename.substring(filename.lastIndexOf(".")+1);
                String newFileName= UUID.randomUUID().toString()+"."+suffix;
                String s = QiniuUpload.updateFile(file, newFileName);
                picturesMapper.addPic(s,repair.getRepairID());
            }
            return ResultVO.success("添加报修记录成功");
        }catch (Exception e){
            e.printStackTrace();
            return ResultVO.failed("添加报修记录失败");
        }
    }


    public ResultVO findRepairByUid(String token) {
        Map<String,Object> hget = (Map<String,Object>)redisUtil.hget(token);
        Integer uid = (Integer) hget.get("uid");
        List<Repair> list=repairMapper.findRepairByUid(uid);
        if (list.size()==0){
            return ResultVO.failed("此用户无报修记录");
        }
        return ResultVO.success(list);
    }

    @Override
    public ResultVO updateSolve(Long repairId) {
        Date date=new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String repair=repairMapper.findRepairByRepairId(repairId);
        if(repair==null){
            return  ResultVO.failed("此记录不存在" );
        }
        repairMapper.updateSolve((String)formatter.format(date),repairId);
        return ResultVO.success("修改成功");
    }

    @Override
    public ResultVO delRepair(Long repairId) {
        String repair=repairMapper.findRepairByRepairId(repairId);
        if(repair==null){
            return  ResultVO.failed("此记录不存在" );
        }
        repairMapper.delRepair(repairId);
        return ResultVO.success("删除成功");
    }

    @Override
    public ResultVO findRepairByRepairId(Long repairId) {
        String repair=repairMapper.findRepairByRepairId(repairId);
        if(repair==null){
            return  ResultVO.failed("此记录不存在" );
        }
        return ResultVO.success(repair);
    }

    @Override
    public List<Repair> findRepair() {
        return repairMapper.findRepair();
    }

    @Override
    public ResultVO pageRepair(int pageNum, int pageSize) {
        Page<Repair> pages = PageHelper.startPage(pageNum, pageSize);
        repairMapper.findRepair();
        return ResultVO.success(pages);
    }

    @Override
    public ResultVO getRepair(Repair repair) {
        String propName=repair.getPropName();
        String location=repair.getLocation();
        String repairSituation=repair.getRepairSituation();
        Integer solve=repair.getSolve();
        String tel=repair.getTel();
        return ResultVO.success(repairMapper.getRepair(propName,location,repairSituation,solve,tel));
    }
}
