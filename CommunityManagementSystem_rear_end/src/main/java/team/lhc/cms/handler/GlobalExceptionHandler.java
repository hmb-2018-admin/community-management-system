package team.lhc.cms.handler;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import team.lhc.cms.components.security.exception.TokenIsExpiredException;
import team.lhc.cms.util.JsonResult;

import java.io.IOException;

/**
 * @author 文
 * @description 全局异常捕捉
 */
@Slf4j
@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {


    private static final String LOG_EXCEPTION_FORMAT = "Capture Exception By GlobalExceptionHandler: Code: %s Detail: %s";
    private static Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 运行时异常
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    public String runtimeExceptionHandler(RuntimeException ex) {
        return resultFormat(1001, ex);
    }

    /**
     * 空指针异常
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler(NullPointerException.class)
    public String nullPointerExceptionHandler(NullPointerException ex) {
        log.error("NullPointerException : ");
        return resultFormat(1002, ex);
    }

    /**
     * 类型转换异常
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler(ClassCastException.class)
    public String classCastExceptionHandler(ClassCastException ex) {
        return resultFormat(1003, ex);
    }

    /**
     * IO异常
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler(IOException.class)
    public String ioExceptionHandler(IOException ex) {
        return resultFormat(1004, ex);
    }

    /**
     * 未知方法异常
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler(NoSuchMethodException.class)
    public String noSuchMethodExceptionHandler(NoSuchMethodException ex) {
        return resultFormat(1005, ex);
    }

    /**
     * 数组越界异常
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler(IndexOutOfBoundsException.class)
    public String indexOutOfBoundsExceptionHandler(IndexOutOfBoundsException ex) {
        return resultFormat(1006, ex);
    }

    /**
     * 400错误:Http消息不可读异常
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler({HttpMessageNotReadableException.class})
    public String requestNotReadable(HttpMessageNotReadableException ex) {
        log.error("400 error -- requestNotReadable : ");
        return resultFormat(1007, ex);
    }

    /**
     * 400错误:类型不匹配异常
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler({TypeMismatchException.class})
    public String requestTypeMismatch(TypeMismatchException ex) {
        log.error("400 error -- TypeMismatchException : ");
        return resultFormat(1008, ex);
    }

    /**
     * 400错误:缺少Servlet请求参数异常
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler({MissingServletRequestParameterException.class})
    public String requestMissingServletRequest(MissingServletRequestParameterException ex) {
        log.error("400 error -- MissingServletRequest : ");
        return resultFormat(1009, ex);
    }

    /**
     * 405错误
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    public String request405(HttpRequestMethodNotSupportedException ex) {
        return resultFormat(1010, ex);
    }

    /**
     * 406错误
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler({HttpMediaTypeNotAcceptableException.class})
    public String request406(HttpMediaTypeNotAcceptableException ex) {
        log.error("406 error : ");
        return resultFormat(1011, ex);
    }

    /**
     * 500错误
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler({ConversionNotSupportedException.class, HttpMessageNotWritableException.class})
    public String server500(RuntimeException ex) {
        log.error("500 error : ");
        return resultFormat(1012, ex);
    }

    /**
     * 栈溢出
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler({StackOverflowError.class})
    public String requestStackOverflow(StackOverflowError ex) {
        return resultFormat(1013, ex);
    }

    /**
     * 除数不能为0
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler({ArithmeticException.class})
    public String arithmeticException(ArithmeticException ex){
        return resultFormat(1014, ex);
    }

    /**
     * 404错误
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler({NotFoundException.class})
    public String notFindException(NotFoundException ex){
        return resultFormat(1015,ex);
    }


    /**
     * 其他系统级错误
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler({Exception.class})
    public String exception(Exception ex) {
        return resultFormat(1016, ex);
    }

    /**
     * token过期异常
     * @author 文
     * @param ex
     * @return
     */
    @ExceptionHandler({TokenIsExpiredException.class})
    public String tokenExpired(TokenIsExpiredException ex){
        return resultFormat(1040,ex);
    }

    /**
     * 捕捉结果格式化
     * @author 文
     * @param code
     * @param ex
     * @param <T>
     * @return
     */
    private <T extends Throwable> String resultFormat(Integer code, T ex) {
        ex.printStackTrace();
        log.error(String.format(LOG_EXCEPTION_FORMAT, code, ex.getMessage()));
        return JsonResult.failed(code, ex.getMessage());
    }

}