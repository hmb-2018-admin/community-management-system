package team.lhc.cms.mapper.provider;

import team.lhc.cms.entity.ParkingSpace;

import java.util.List;
import java.util.Map;

/**
 * @author 文
 * @description 车位的sql加工类
 */
public class BatchParkingSpace {

    /**
     * 批量导入车位信息
     * @author 文
     * @param map
     * @return
     */
    public String batSave(Map<String,Object> map){
        List<ParkingSpace> data = (List<ParkingSpace>) map.get("collection");
        StringBuffer sb = new StringBuffer();
        sb.append("INSERT INTO parking_space(pid,type,status) VALUES");
        for (int i = 0; i < data.size(); i++) {
            sb.append("('" + data.get(i).getPid() + "','" + data.get(i).getType() +  "','" + data.get(i).getStatus() + "')");
            if (i == data.size()-1){
                sb.append(";");
            }else {
                sb.append(",");
            }
        }
        return sb.toString();
    }
}
