package team.lhc.cms.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import team.lhc.cms.entity.Decoration;
import team.lhc.cms.entity.Pictures;
import java.util.List;

@Mapper
public interface PicturesMapper {
    @Select("select url from pictures where did=#{did}")
     List<Pictures> getPicturesByDid(int did);

    @Select("select url from pictures where repair_id=#{repairID}")
     List<Pictures> getPicturesByRepairID(int repairID);

    @Insert("insert into pictures (url,did) values(#{url},#{did})")
     void insertPic(String url,Integer did);

    @Insert("insert into pictures (url,repair_id) values(#{url},#{repairID})")
     void addPic(String url,Integer repairID);
}
