package team.lhc.cms.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import team.lhc.cms.entity.License;

import java.util.List;

/**
 * @author 文
 * @description 车牌数据操作层
 */
@Repository
public interface LicenseMapper {

    /**
     * 插入一条只有uid(其他为null)的记录
     * @author 文
     * @param uid
     */
    @Insert("INSERT INTO `license` (`uid`) VALUES (#{uid})")
    void iniLincense(Integer uid);

    /**
     * 根据uid寻找记录
     * @author 文
     * @param uid
     * @return
     */
    @Select("select * from license where uid = #{uid}")
    List<License> getListByUid(Integer uid);

    /**
     * 根据lid设置车牌号
     * @author 文
     * @param lid
     * @param license
     */
    @Update("UPDATE `license` SET `car_license`= #{license} WHERE (`lid`=#{lid})")
    void setLicenseByLid(Integer lid, String license);

    /**
     * 根据车牌号找记录
     * @author 文
     * @param license
     * @return
     */
    @Select("select * from license where car_license = #{license}")
    License findByLicense(String license);
}
