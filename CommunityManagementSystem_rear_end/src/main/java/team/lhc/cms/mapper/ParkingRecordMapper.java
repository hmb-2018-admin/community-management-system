package team.lhc.cms.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import team.lhc.cms.entity.ParkingRecord;

/**
 * @author 文
 * @description 停车记录数据操作层
 */
@Repository
public interface ParkingRecordMapper {
    /**
     * 根据车牌号找最近的一条记录
     * @author 文
     * @param license
     * @return
     */
    @Select("select * from parking_record where license = #{license} order by rid desc limit 0,1")
    ParkingRecord findRecentRecordByLicense(String license);

    /**
     * 新建一条拥有车牌号、车位号、入场时间的一条停车记录（用于车辆入场）
     * @author 文
     * @param parkingRecord
     */
    @Insert("INSERT INTO `parking_record` (`license`, `pid`, `entry_time`) VALUES (#{license}, #{pid}, #{entryTime})")
    void save(ParkingRecord parkingRecord);

    /**
     * 根据rid更新该停车记录的出场时间和费用（用于车辆出场）
     * @author 文
     * @param rid
     * @param departureTime
     * @param fee
     */
    @Update("UPDATE `parking_record` SET `departure_time`= #{departureTime}, `fee`=#{fee} WHERE (`rid`=#{rid})")
    void updateDepartureTimeAndFee(Integer rid, String departureTime, String fee);
}
