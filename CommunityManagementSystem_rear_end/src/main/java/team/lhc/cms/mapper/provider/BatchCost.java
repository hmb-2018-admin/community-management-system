package team.lhc.cms.mapper.provider;

import team.lhc.cms.entity.Cost;

import java.util.List;
import java.util.Map;

/**
 * @author 文
 * @description 缴费表的sql加工类
 */
public class BatchCost {

    /**
     * 批量导入缴费信息
     * @author 文
     * @param map
     * @return
     */
    public String batSave(Map<String,Object> map){
        List<Cost> data = (List<Cost>) map.get("collection");
        StringBuffer sb = new StringBuffer();
        sb.append("INSERT INTO `cost` (`room_id`, `car_fee`, `pro_fee`, `el_fee`, `el_degree`," +
                " `water_fee`, `water_tonnes`, `gas_fee`, `gas_vol`, `total`, `c_time`) VALUES");
        for (int i = 0; i < data.size(); i++) {
            sb.append("('" + data.get(i).getRoomId() + "','" + data.get(i).getCarFee() + "','" + data.get(i).getProFee() + "','" + data.get(i).getElFee() + "','"
                     + data.get(i).getElDegree() + "','" + data.get(i).getWaterFee() + "','" + data.get(i).getWaterTonnes() + "','" + data.get(i).getGasFee() + "','"
                     + data.get(i).getGasVol() + "','" + data.get(i).getTotal() + "','" + data.get(i).getCTime() + "')");
            if (i == data.size()-1){
                sb.append(";");
            }else {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    /**
     * 根据条件搜索账单
     * @param map
     * @return
     */
    public String conditionSelect(Map<String,Object> map){
        String roomId = (String) map.get("roomId");
        String beginTime = (String) map.get("beginTime");
        String endTime = (String) map.get("endTime");
        String isPay = (String) map.get("isPay");
        Boolean flag = false;
        StringBuffer sb = new StringBuffer();

        sb.append("select * from cost");
        //无条件查询
        if ("-1".equals(isPay) && "-1".equals(roomId) && "-1".equals(beginTime) && "-1".equals(endTime)){
            sb.append(" order by c_time asc");
            return sb.toString();
        }
        sb.append(" where ");
        //房号查询
        if (!"-1".equals(roomId)){
            sb.append("room_id = '" + roomId + "' ");
            flag = true;
        }

        //付费状态查询
        if (!"-1".equals(isPay)){
            if (Boolean.TRUE.equals(flag)){
                sb.append(" and ");
            }
            sb.append("status = '" + isPay + "'");
            flag = true;
        }

        //时间范围查询
        //两个时间内
        if (!"-1".equals(beginTime) && !"-1".equals(endTime)){
            if (Boolean.TRUE.equals(flag)){
                sb.append(" and ");
            }
            sb.append(" c_time between '" + beginTime + "'" + " and '" + endTime + "'");
        }else if (!"-1".equals(beginTime)){
            if (Boolean.TRUE.equals(flag)){
                sb.append(" and ");
            }
            sb.append(" c_time >= '" + beginTime + "'");
        }else if (!"-1".equals(endTime)){
            if (Boolean.TRUE.equals(flag)){
                sb.append(" and ");
            }
            sb.append(" c_time <= '" + beginTime + "'");
        }


        sb.append(" order by c_time asc");
        return sb.toString();
    }
}
