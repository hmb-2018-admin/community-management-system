package team.lhc.cms.mapper;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import team.lhc.cms.entity.Decoration;

import java.util.List;

@Mapper
public interface DecorationMapper {
    @Select("<script> SELECT * from decoration INNER JOIN proprietor on decoration.uid=proprietor.uid" +
            "<where>"+
            "<if test='propName != null'>and prop_name like '%${propName}%'</if> "+
            " <if test='description != null'>and description like '%${description}%'</if> "+
            " <if test='solve != null'>and solve = #{solve}</if> "+
            " <if test='tel != null'>and tel like '${tel}%'</if> "+
            "</where>"+
            "</script>")
    @Results({
            @Result(id=true,column="did",property="did"),
            @Result(column="description",property="description"),
            @Result(column="uid",property="uid"),
            @Result(column="solve",property="solve"),
            @Result(column="did",property="urls",
                    many=@Many(
                            select="team.lhc.cms.mapper.PicturesMapper.getPicturesByDid",
                            fetchType= FetchType.EAGER
                    )
            ),
            @Result(column="uid",property="propName",
                    many=@Many(
                            select="team.lhc.cms.mapper.ProprietorMapper.findNameByUid",
                            fetchType= FetchType.EAGER
                    )
            ),
            @Result(column="uid",property="tel",
                    many=@Many(
                            select="team.lhc.cms.mapper.ProprietorMapper.findTelByUid",
                            fetchType= FetchType.EAGER
                    )
            )
    })
     List<Decoration> getDecoration(@Param("description") String description,
                                    @Param("propName") String propName,
                                    @Param("solve") Integer solve,
                                    @Param("tel") String tel);

    @Insert("insert into decoration (description,solve,uid) values(#{description},#{solve},#{uid})")
    @Options(useGeneratedKeys=true, keyProperty="did", keyColumn="did")
    void insertDecoration(Decoration decoration);

    @Select("select * from decoration where uid=#{uid}")
    Decoration selectDecorationByUid(Integer uid);

    @Update("update decoration set solve=1 where did=#{did}")
    void putDecoration(String did);

    @Delete("delete from decoration where did=#{did}")
    void delDecoration(String did);



}
