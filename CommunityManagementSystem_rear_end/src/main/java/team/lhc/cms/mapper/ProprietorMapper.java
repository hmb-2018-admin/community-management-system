package team.lhc.cms.mapper;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import team.lhc.cms.entity.Proprietor;
import java.util.List;
import java.util.Map;

@Repository
public interface ProprietorMapper {

    //查询全部业主
    @Select("select * from proprietor")
    List<Proprietor> findAll();

    //通过id查询业主
    @Select("select * from proprietor where pid=#{uid}")
    Proprietor findById(@Param("uid") int pid);

    //通过名字查询业主
    @Select("select * from proprietor where prop_name=#{name}")
    Proprietor findByName(@Param("name") String name);

    //通过房号查询业主
    @Select("select * from proprietor where room_id=#{roomId}")
    Proprietor findByRoomId(@Param("roomId") int roomId);

    //分页
    @Select("select * from proprietor limit #{page},#{size}")
    List<Proprietor> findByPage(Map<String, Object> params);

    //查询业主总数
    @Select("select count(*) from proprietor")
    int totalPro();

    //添加业主
    @Insert("insert into proprietor (room_id,prop_name,id_card,tel,car_id,car_type,uid) values (#{roomId},#{propName},#{idCard},#{tel},#{carId},#{carType},#{uid})")
    int insertPro(Proprietor proprietor);

    //通过id删除业主
    @Delete("delete from proprietor where pid=#{pid}")
    int deleteProById(@Param("pid")int pid);

    //通过名字删除业主
    @Delete("delete from proprietor where prop_name=#{name}")
    int deleteProByName(@Param("name")String name);

    //通过房号删除业主
    @Delete("delete from proprietor where room_id=#{roomId}")
    int deleteProByRoomId(@Param("roomId")int roomId);

    //更新业主信息
    @Update("update proprietor set room_id=#{roomId},prop_name=#{propName},id_card=#{idCard},tel=#{tel},car_id=#{carId},car_type=#{carType},uid=#{uid} where pid=#{pid}")
    int updatePro(Proprietor proprietor);

    /**
     * 获取所有房号
     * @author 文
     * @return
     */
    @Select("SELECT room_id from proprietor GROUP BY room_id ASC")
    List<String> getAllRoom();

    /**
     * 根据uid寻找业主信息
     * @author 文
     * @param uid
     * @return
     */
    @Select("select * from proprietor where uid = #{uid}")
    Proprietor findByUid(Integer uid);

    /**
     * 根据电话和身份证寻找业主信息
     * @author 文
     * @param tel
     * @param idCard
     * @return
     */
    @Select("select * from proprietor where tel = #{tel} and id_card = #{idCard}")
    Proprietor findByTelAndIdCard(String tel, String idCard);

    /**
     * 根据tel和idCard添加uid
     * @author 文
     * @param uid
     * @param tel
     * @param idCard
     */
    @Update("UPDATE `proprietor` SET `uid`= #{uid} WHERE tel = #{tel} and id_card = #{idCard}")
    void updateUidByTelAndIdCard(Integer uid, String tel, String idCard);

    /**
     * 根据uid设置uid为空
     * @author 文
     * @param uid
     * @return
     */
    @Update("UPDATE `proprietor` SET `uid`= NULL WHERE uid = #{uid}")
    int setUidNullByUid(Integer uid);

    /**
     * 根据uid更新手机号
     * @author 文
     * @param tel
     * @param uid
     * @return
     */
    @Update("UPDATE `proprietor` SET `tel`= #{tel} WHERE uid = #{uid}")
    int updateTelByUid(String tel, Integer uid);

    /**
     * 根据uid查名字
     * @param uid
     * @return
     */
    @Select("select prop_name from proprietor where uid=#{uid}")
    String findNameByUid(Integer uid);

    /**
     * 根据uid查电话
     * @param uid
     * @return
     */
    @Select("select tel from proprietor where uid=#{uid}")
    String findTelByUid(Integer uid);
}
