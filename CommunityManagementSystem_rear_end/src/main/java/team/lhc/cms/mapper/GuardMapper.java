package team.lhc.cms.mapper;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import team.lhc.cms.entity.Guard;

@Repository
public interface GuardMapper {
    /**
     * 根据uid寻找保安
     * @author 文
     * @param uid
     * @return
     */
    @Select("select * from guard where uid = #{uid}")
    Guard findByUid(Integer uid);

    /**
     * 根据电话和身份证寻找保安信息
     * @author 文
     * @param tel
     * @param idCard
     * @return
     */
    @Select("select * from guard where tel = #{tel} and id_card = #{idCard}")
    Guard findByTelAndIdCard(String tel, String idCard);

    /**
     * 更新保安信息
     * @author 文
     * @param guard
     */
    @Update("UPDATE `guard` SET `guard_name`=#{guardName}, `tel`=#{tel}, `guard_period`=#{guardPeriod}, `id_card`=#{idCard}, `uid`=#{uid} WHERE (`guard_id`=#{guardId})")
    void updateGuard(Guard guard);

    /**
     * 根据uid设uid为空
     * @author 文
     * @param uid
     * @return
     */
    @Update("UPDATE `guard` SET `uid`= NULL WHERE uid = #{uid}")
    int setUidNullByUid(Integer uid);
}
