package team.lhc.cms.mapper;

import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;
import team.lhc.cms.entity.Cost;
import team.lhc.cms.mapper.provider.BatchCost;

import java.util.List;



@Repository
public interface CostMapper {

    /**
     * 批量插入缴费记录
     * @author 文
     * @param costs
     * @return
     */
    @InsertProvider(value = BatchCost.class,method = "batSave")
    int batSave(List<Cost> costs);

    /**
     * 根据costId寻找账单
     * @author 文
     * @param costId
     * @return
     */
    @Select("select * from cost where cost_id = #{costId}")
    Cost fineByCostId(Integer costId);

    /**
     * 根据房号寻找账单
     * @author 文
     * @param roomId
     * @return
     */
    @Select("select * from cost where room_id like '%#{roomId}%' order by cost_id desc")
    List<Cost> getListByRoomId(String roomId);

    /**
     * 根据条件(房号或日期)查询账单
     * @author 文
     * @param roomId
     * @param beginTime
     * @param endTime
     * @param isPay
     * @return
     */
    @SelectProvider(value = BatchCost.class,method = "conditionSelect")
    List<Cost> findByCondition(String roomId, String beginTime, String endTime, String isPay);
}
