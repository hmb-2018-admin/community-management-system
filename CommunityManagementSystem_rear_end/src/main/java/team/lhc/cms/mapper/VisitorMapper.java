package team.lhc.cms.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import team.lhc.cms.entity.Visitor;

import java.util.List;


@Repository
public interface VisitorMapper {

    /**
     * 存储访客记录
     * @author 文
     * @param visitor
     * @return
     */
    @Insert("INSERT INTO `visitor` (`visitor_number`, `reason`, `visitor_name`, `visitor_tel`, `room_id`, `visit_time`,`days`,`temp_license`, `uuid`, `uid`)" +
            " VALUES (#{visitorNumber},#{reason}, #{visitorName},#{visitorTel}, #{roomId}, #{visitTime}, #{days}, #{tempLicense},#{uuid}, #{uid})")
    @Options(useGeneratedKeys=true, keyProperty="visitId", keyColumn="visit_id")
    void save(Visitor visitor);

    /**
     * 按来访时间查询记录
     * @author 文
     * @param time
     * @return
     */
    @Select("select * from visitor where visit_time = #{time}")
    List<Visitor> getListByDate(String time);

    /**
     * 按uid查询记录
     * @author 文
     * @param uid
     * @return
     */
    @Select("select * from visitor where uid = #{uid}")
    List<Visitor> getListByUid(Object uid);

    /**
     * 根据visitId查询记录
     * @author 文
     * @param visitId
     * @return
     */
    @Select("select * from visitor where visit_id = #{visitId}")
    Visitor findByVisitId(String visitId);
}
