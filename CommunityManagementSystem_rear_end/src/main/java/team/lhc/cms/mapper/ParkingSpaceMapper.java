package team.lhc.cms.mapper;


import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import team.lhc.cms.dto.LegalParking;
import team.lhc.cms.entity.ParkingSpace;
import team.lhc.cms.mapper.provider.BatchParkingSpace;

import java.util.List;

/**
 * @author 文
 * @description 车位数据操作层
 */
@Repository
public interface ParkingSpaceMapper {


    /**
     * 更新某个车位信息
     * @author 文
     * @param parkingSpace
     * @return
     */
    @Update("UPDATE `parking_space` SET `type`=#{type}, `status`=#{status}, `uid`=#{uid} WHERE (`pid`=#{pid})")
    Integer updateByPid(ParkingSpace parkingSpace);

    /**
     * 寻找一个临时车位
     * @author 文
     * @return
     */
    @Select("select * from parking_space where type = 0 and status = 0 limit 0,1")
    ParkingSpace findOneFreeOnTemp();

    /**
     * 寻找一个非临时的空闲车位
     * @author 文
     * @return
     */
    @Select("select * from parking_space where (type = 1 or 2) and status = 0 and uid = -1")
    ParkingSpace findOneFreeOnOther();

    /**
     * 寻找合法的停车信息(为空即不存在该车牌或空停车位)
     * @author 文
     * @param license
     * @return
     */
    @Select("SELECT lid,car_license,license.uid,pid,type,`status` from license" +
            " LEFT JOIN parking_space ON license.uid = parking_space.uid" +
            " WHERE car_license = #{license} AND `status` = 0 limit 0,1")
    List<LegalParking> findLegalParking(String license);

    /**
     * 根据pid更新状态
     * @author 文
     * @param status
     * @param pid
     */
    @Update("UPDATE `parking_space` SET `status`=#{status} WHERE (`pid`=#{pid})")
    void updateStatusByPid(int status, String pid);

    /**
     * 导入车位
     * @author 文
     * @param parkingSpaces
     * @return
     */
    @InsertProvider(type = BatchParkingSpace.class,method = "batSave")
    Integer batchSave(List<ParkingSpace> parkingSpaces);

    /**
     * 获取停车位信息
     * @author 文
     * @return
     */
    @Select("select * from parking_space order by pid desc")
    List<ParkingSpace> getParkingSpaceList();
}
