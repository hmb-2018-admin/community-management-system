package team.lhc.cms.mapper;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import org.springframework.stereotype.Repository;
import team.lhc.cms.entity.Repair;

import java.rmi.server.UID;
import java.util.List;
import java.util.Map;

@Repository
public interface RepairMapper {
    /**
     * 添加报修情况
     */
//    @Options(useGeneratedKeys = true,keyProperty = "repairId")
    @Insert("insert into repair (location,repair_situation,solve,time,uid,repair_id) values (#{location},#{repairSituation},#{solve},#{time},#{uid},#{repairID})")
    @Options(useGeneratedKeys=true, keyProperty="repairID", keyColumn="repairID")
    void addRepair(Repair repair);

    /**
     * 查询报修情况
     * @param uid
     * @return
     */
    @Select("SELECT * FROM repair WHERE uid=#{uid} ")
    List<Repair> findRepairByUid(@Param("uid") Integer uid);

    /**
     * 状态更新为已维修
     * @param repairId
     */
    @Update("update repair set solve = 1,time= #{time} where repair_id = #{repairId}")
    void updateSolve(@Param("time") String time,@Param("repairId") Long repairId);

    /**
     * 删除报修记录
     * @param repairId
     */
    @Delete("delete from repair where repair_id =#{repairId}")
    void delRepair(@Param("repairId") Long repairId);

    @Select("SELECT * FROM repair WHERE repair_id=#{repairId} ")
    String findRepairByRepairId(@Param("repairId") Long repairId);

    @Select("select * from repair")
    @Results(value = {
            @Result(id=true,column="repair_id",property="repairID"),
            @Result(column="location",property="location"),
            @Result(column="repair_situation",property="repairSituation"),
            @Result(column="time",property="time"),
            @Result(column="uid",property="uid"),
            @Result(column="repair_id",property="urls",
                    many=@Many(
                            select="team.lhc.cms.mapper.PicturesMapper.getPicturesByRepairID",
                            fetchType= FetchType.EAGER
                    )
            ),
            @Result(column="uid",property="propName",
                    many=@Many(
                            select="team.lhc.cms.mapper.ProprietorMapper.findNameByUid",
                            fetchType= FetchType.EAGER
                    )
            ),
            @Result(column="uid",property="tel",
                    many=@Many(
                            select="team.lhc.cms.mapper.ProprietorMapper.findTelByUid",
                            fetchType= FetchType.EAGER
                    )
            )
    })
    List<Repair> findRepair();


    @Select("<script> SELECT * from repair INNER JOIN proprietor on repair.uid=proprietor.uid" +
            "<where>"+
            "<if test='propName != null'>and prop_name like '%${propName}%'</if> "+
            " <if test='location != null'>and location like '%${location}%'</if> "+
            " <if test='repairSituation != null'>and repair_situation like '%${repairSituation}%'</if>" +
            " <if test='solve != null'>and solve like '%${solve}%'</if> "+
            " <if test='tel != null'>and tel like '${tel}%'</if> "+
            "</where>"+
            "</script>")
    @Results(value = {
            @Result(id=true,column="repair_id",property="repairID"),
            @Result(column="location",property="location"),
            @Result(column="repair_situation",property="repairSituation"),
            @Result(column="time",property="time"),
            @Result(column="uid",property="uid"),
            @Result(column="repair_id",property="urls",
                    many=@Many(
                            select="team.lhc.cms.mapper.PicturesMapper.getPicturesByRepairID",
                            fetchType= FetchType.EAGER
                    )
            ),
            @Result(column="uid",property="propName",
                    many=@Many(
                            select="team.lhc.cms.mapper.ProprietorMapper.findNameByUid",
                            fetchType= FetchType.EAGER
                    )
            ),
            @Result(column="uid",property="tel",
                    many=@Many(
                            select="team.lhc.cms.mapper.ProprietorMapper.findTelByUid",
                            fetchType= FetchType.EAGER
                    )
            )
    })
    List<Repair> getRepair(@Param("propName") String propName,
                           @Param("location") String location,
                           @Param("repairSituation") String repairSituation,
                           @Param("solve") Integer solve,
                           @Param("tel") String tel);

}
