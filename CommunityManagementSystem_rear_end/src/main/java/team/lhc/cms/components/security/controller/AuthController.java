package team.lhc.cms.components.security.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import team.lhc.cms.components.security.service.WeChatService;
import team.lhc.cms.components.security.utils.JwtTokenUtils;
import team.lhc.cms.service.UserService;
import team.lhc.cms.util.JsonResult;
import team.lhc.cms.util.RedisUtil;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author 文
 * @description 权限的相关视图层
 */
@Api(tags = "权限")
@RestController
public class AuthController {

    @Autowired
    private WeChatService weChatService;

    @Autowired
    private UserService userService;

    @Autowired
    RedisUtil redisUtil;

    /**
     * 注册用户
     * @author 文
     * @param registerUser 获取username,password,role
     * @return
     */
    @ApiOperation("注册用户")
    @PostMapping("/auth/register")
    public String registerUser(@RequestBody Map<String,String> registerUser){
        return userService.register(registerUser);
    }

    /**
     * 微信登录
     * @author 文
     * @param code
     * @return
     * @throws Exception
     */
    @ApiOperation("微信登录")
    @PostMapping("/auth/weChatLogin")
    public String weChatLogin(String code) throws Exception {
        return weChatService.weChatLogin(code);
    }

    /**
     * 完善(认证)用户信息
     * @author 文
     * @param nickName
     * @param tel
     * @param idCard
     * @param role
     * @param request
     * @return
     */
    @ApiOperation("完善(认证)用户信息")
    @PostMapping("/public/completeInfo")
    public String completeInfo(String nickName, String tel, String idCard, Integer role, HttpServletRequest request){
        String token = request.getHeader(JwtTokenUtils.TOKEN_HEADER);
        return weChatService.completeInfo(nickName, tel, idCard, role, token);
    }


    /**
     * 完善用户名
     * @author 文
     * @param map
     * @return
     * @throws Exception
     */
    @ApiOperation("完善用户名")
    @PostMapping("/public/completeUsername")
    public String completeUsername(@RequestBody Map<String,Object> map) throws Exception {
        return weChatService.completeUsername(map);
    }


    /**
     * 登出
     * @author 文
     * @param request
     * @return
     */
    @ApiOperation("登出")
    @PostMapping("/public/logout")
    public String logout(HttpServletRequest request){
        String token = request.getHeader(JwtTokenUtils.TOKEN_HEADER);
        Boolean hasKey = redisUtil.hasKey(token);
        if (Boolean.FALSE.equals(hasKey)){
            return JsonResult.success("登出成功");
        }
        redisUtil.delete(token);
        return redisUtil.hasKey(token) ? JsonResult.failed("登出失败") : JsonResult.success("登出成功");
    }

    /**
     * 用户解绑
     * @author 文
     * @param request
     * @return
     */
    @ApiOperation("用户解绑")
    @PostMapping("/public/untie")
    public String untie(String role, HttpServletRequest request){
        String token = request.getHeader(JwtTokenUtils.TOKEN_HEADER);
        return userService.untie(role, token);
    }
}
