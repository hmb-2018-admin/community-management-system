package team.lhc.cms.components.security.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import team.lhc.cms.components.security.entity.JwtUser;
import team.lhc.cms.entity.User;
import team.lhc.cms.mapper.UserMapper;


/**
 * @author 文
 * @description 用于security获取用户对象
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userMapper.findByUsername(s);
        return new JwtUser(user);
    }

}
