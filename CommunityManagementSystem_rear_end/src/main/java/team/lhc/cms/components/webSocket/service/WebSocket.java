//package team.lhc.cms.components.webSocket.service;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import team.lhc.cms.util.RedisUtil;
//
//import javax.websocket.OnClose;
//import javax.websocket.OnMessage;
//import javax.websocket.OnOpen;
//import javax.websocket.Session;
//import javax.websocket.server.PathParam;
//import javax.websocket.server.ServerEndpoint;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.CopyOnWriteArraySet;
//
///**
// * @author 文
// * @description
// * @remark "@ServerEndpoint"为配置连接URL(站点连接)
// */
//@Slf4j
//@Component
//@ServerEndpoint("/websocket/{username}")
//public class WebSocket {
//
//    private Session session;
//
//    @Autowired
//    RedisUtil redisUtil;
//
//    private static CopyOnWriteArraySet<WebSocket> webSockets =new CopyOnWriteArraySet<>();
//    private static Map<String, Session> sessionPool = new HashMap<String, Session>();
//
//    @OnOpen
//    public void onOpen(Session session,@PathParam("username") String username) {
//        this.session = session;
//        webSockets.add(this);
//        sessionPool.put(username, session);
//        log.info("【websocket消息】有新的连接，总数为:"+webSockets.size());
//
//    }
//
//    @OnClose
//    public void onClose() {
//        webSockets.remove(this);
//        log.info("【websocket消息】连接断开，总数为:"+webSockets.size());
//    }
//
//    @OnMessage
//    public void onMessage(String message) {
//        log.info("【websocket消息】收到客户端消息:"+message);
//    }
//
//    // 此为广播消息
//    public void sendAllMessage(String message) {
//        for(WebSocket webSocket : webSockets) {
//            log.info("【websocket消息】广播消息:"+message);
//            try {
//                webSocket.session.getAsyncRemote().sendText(message);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    // 此为单点消息 (发送文本)
//    public void sendTextMessage(String username, String message) {
//        Session session = sessionPool.get(username);
//        if (session != null) {
//            try {
//                session.getBasicRemote().sendText(message);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    // 此为单点消息 (发送对象)
//    public void sendObjMessage(String username, Object message) {
//        Session session = sessionPool.get(username);
//        if (session != null) {
//            try {
//                session.getAsyncRemote().sendObject(message);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//}
//
