package team.lhc.cms.components.security.service;

import java.util.Map;

/**
 * @author 文
 * @description 微信服务接口类
 */
public interface WeChatService {

    /**
     * 小程序登录
     * @author 文
     * @param code
     * @return
     * @throws Exception
     */
    String weChatLogin(String code)throws Exception;


    /**
     * 完善用户名信息
     * @author 文
     * @param map
     * @return
     * @throws Exception
     */
    String completeUsername(Map<String, Object> map) throws Exception;

    /**
     * 完善(认证)用户信息
     * @author 文
     * @param nickName
     * @param tel
     * @param idCard
     * @param role
     * @return
     */
    String completeInfo(String nickName, String tel, String idCard, Integer role,String token);
}
