package team.lhc.cms;

import com.alibaba.fastjson.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import team.lhc.cms.components.security.utils.HttpUtil;
import team.lhc.cms.controller.PayController;
import team.lhc.cms.entity.User;
import team.lhc.cms.mapper.CostMapper;
import team.lhc.cms.mapper.UserMapper;
import team.lhc.cms.util.PassCode;
import team.lhc.cms.util.RedisUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@SpringBootTest
class CmsApplicationTests {
//    @Value("${jwt.header}")
//    private String test;

//    @Autowired
//    RedisUtil redisUtil;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    UserMapper userMapper;

    @Autowired
    PayController payController;

    @Autowired
    CostMapper costMapper;


    @Test
    void contextLoads() throws Exception {
//        System.out.println(PassCode.createPassCode());
//        User user = new User();
//        user.setOpenid("dsadsafa");
//        userMapper.save(user);
//        System.out.println("uid = " + user.getId());
//        System.out.println(payController.lookOverPaymentListByCondition("-1","2020-1","2020-4"));
//        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
////        String EncryptedPassword = bCryptPasswordEncoder.encode("test123");
////        System.out.println(EncryptedPassword);
//        redisUtil.set("test","123");
//        System.out.println(redisUtil.get("test"));
//        System.out.println(redisUtil.hasKey("test"));
//        System.out.println(redisUtil.hasKey("test1"));
////        redisTemplate.opsForValue().set("test","123");
////        System.out.println(redisTemplate.opsForValue().get("test"));
//        System.out.println(HttpUtil.sendGet("localhost:8080/map"));
//        Map<String,Object> map = new HashMap<>();
//        map.put("test",1);
//        map.put("data","这是一条测试数据");
//        redisUtil.hset("testhset", (HashMap<String, Object>) map);
////        JSONObject testhset = JSONObject.parseObject((String) );
//        Map<String,Object> testhset1 = (Map<String, Object>) redisUtil.hget("testhset");
////        System.out.println(testhset.getString("data"));
//        System.out.println(testhset1.get("data"));
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd");
//        System.out.println(simpleDateFormat.format(new Date()));
//        Date d1 = simpleDateFormat.parse("2020-10-20 16:32");
//        Date d2 = simpleDateFormat.parse("2020-10-16");
//        long diff = d1.getTime() - d2.getTime();
//        System.out.println(diff/1000/60/60/24);
//        System.out.println(redisUtil.hasKey("tempLicenses"));
//        System.out.println(redisUtil.hasKey("tempLicenses","川Q66667"));
//
//        System.out.println(redisUtil.hget("tempLicense"));
//        redisUtil.hset("tempLicense","川Q66666","R602",100000L);
//        System.out.println(redisUtil.hget("tempLicense"));
//        System.out.println(redisUtil.hget("tempLicense","川Q66666"));
        //Integer numByRoomId = costMapper.getNumByRoomId();
        //System.out.println(numByRoomId);

//        Map<String,Object> map = new HashMap<>();
//        map.put("a",2);
//        map.put("a",3);
//        System.out.println(map.get("a"));
    }

}
