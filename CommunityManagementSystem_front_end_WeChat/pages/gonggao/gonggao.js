// pages/gonggao/gonggao.js
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        //获取数据库数据
        let that = this;
        wx.request({
          url: 'url',    //接口地址
          data:{},
          method:'GET',
          header:{
              'content-Type':'application/json'
          },
          success:function(res){
              that.setData({
                  data:res.data,
              });
          },
          fail:function(err){
            console.log("失败" + err)
          }
        })

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    onOpenItem: function(event) {
        var index = event.currentTarget.dataset.index;
        wx.navigateTo({
          url: '../gonggao/gonggao-detail?index=' + index,
        })
      }

})