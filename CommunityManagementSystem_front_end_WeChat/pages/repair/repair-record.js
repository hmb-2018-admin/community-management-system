const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    repairList:[],
    hasRepair:"",
    // uid:""
  },

  /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      var that = this
          wx.request({   
            url: app.globalData.baseUrl+'/public/repair',//接口地址   (带uid +res.data.data.uid)   
            method:"GET", 
            header: {         
              'content-type': 'application/json',           
              'Authorization': app.globalData.Authorization
            },      
            success: function (res) {  
              console.log("返回信息：",res.data) 
              if(res.data.code == 101){
              that.setData({
                hasRepair:true,
                repairList:res.data.data
              })   
            }else{
              that.setData({
                hasRepair:false
              }) 
            }
            }
          }) 
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
   

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})