const app = getApp()
var adds = {}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    img_arr: [],
    formdata: '',

  },

  //数据提交
  formSubmit: function (e) {
    adds = e.detail.value;
    this.upload()
  },

  //上传图片和信息
  upload: function () {
    var that = this
    for (var i = 0; i < that.data.img_arr.length; i++) {
      wx.uploadFile({
        url: app.globalData.baseUrl + '/public/repair',
        method: "POST",
        header: {
          'content-type': 'multipart/form-data',
          'Authorization': app.globalData.Authorization
        },
        filePath: that.data.img_arr[i],
        name: 'pic',
        formData: adds,
        success: function (res) {
          console.log("提交状态:", res.data)
          var data = JSON.parse(res.data)
          console.log("data:", data)
          if (data.code == 101) {
            wx.showToast({
              title: '提交成功',
              duration: 2000,
              success: function (res) {
                setTimeout(function () { //延迟跳转
                  wx.switchTab({
                    url: '../index/index'
                  })
                }, 2000)
              }
            })
          } else {
            wx.showToast({
              title: '提交失败',
              image: "../../icon/fail.png"
            })
          }
        }
      })
    }
    // this.setData({
    //   formdata: ''
    // })
  },

  //选择图片
  upimg: function () {
    var that = this;
    wx.chooseImage({
      // count: 9,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        that.setData({
          img_arr: that.data.img_arr.concat(res.tempFilePaths)
        })
        console.log("选择图片张数：",that.data.img_arr.length)
      }
    })
  },

  // 预览图片
  previewImg: function (e) {
    //获取当前图片的下标
    var index = e.currentTarget.dataset.idx;
    var imgs = this.data.img_arr;
    wx.previewImage({
      //显示图片
      current: imgs[index],
      urls: imgs
    })
  },

  // 删除图片
  deleteImg: function (e) {
    // debugger;
    var imgs = this.data.img_arr;
    var index = e.currentTarget.dataset.index;
    imgs.splice(index, 1);
    this.setData({
      imgs: imgs
    })
    console.log("删除图片剩余张数：", imgs.length)
  },

  //查看报修记录
  record: function () {
    wx.navigateTo({
      url: '../repair/repair-record',
    })
  }
})