// pages/jiaofei/record/record.js
var util = require('../../utils/util.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        date:"2020-10",
        orderList:[
          { id:1,name: '电费',liang:"用电量：",value:"78度",time:"03/12-04/12",price:78.02},
          { id:2,name: '水费',liang:"用水量：",value:"3吨",time:"03/12-04/12",price:78.52},
          { id:3,name: '物管费',time:"03/12-04/12",price:178.00},
          { id:4,name: '车位管理费',time:"03/12-04/12",price:58.42},
          { id:5,name: '燃气费',liang:"用水量：",value:"45",time:"03/12-04/12",price:110.02}
        ]
    },


    bindDateChange: function (e) {
        console.log('日期为', e.detail.value)
        this.setData({
          date: e.detail.value
        })
    
      },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
         // 调用函数时，传入new Date()参数，返回值是日期和时间
    var time = util.formatYear(new Date());
    // 再通过setData更改Page()里面的data，动态更新页面的数据
    this.setData({
      time: time
    });
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})