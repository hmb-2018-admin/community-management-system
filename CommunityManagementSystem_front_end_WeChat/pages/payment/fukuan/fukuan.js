// pages/jiaofei/fukuan/fukuan.js

Page({

    /**
     * 页面的初始数据
     */
    data: {
      total:0,
      orderList:[
        { id:1,name: '电费',datatime:"10-10 20:21",price:78.02},
        { id:2,name: '水费',datatime:"10-10 20:21",price:78.52},
        { id:3,name: '物业费',datatime:"10-10 20:21",price:178.00},
        { id:4,name: '垃圾费',datatime:"10-10 20:21",price:58.42},
        { id:5,name: '燃气费',datatime:"10-10 20:21",price:110.02}
      ]
    },
      /**
   * 计算总价
   */
  getTotalPrice() {
    let orderList = this.data.orderList;                  // 获取购物车列表
    let total = 0;
    for(let i = 0; i<orderList.length; i++) {         // 循环列表得到每个数据
      if(orderList[i].selected) {                     // 判断选中才会计算价格
        total +=  orderList[i].price;   // 所有价格加起来
      }
    }
    this.setData({                                // 最后赋值到data中渲染到页面
      orderList: orderList,
      totalPrice: total.toFixed(2)
    });
  },
  toPay() {
    wx.showModal({
      title: '提示',
      content: '本系统只做演示，支付系统已屏蔽',
      text:'center',
      complete() {
        wx.switchTab({
          url: '/page/component/user/user'
        })
      }
    })
  },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {
        this.getTotalPrice();
      },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})