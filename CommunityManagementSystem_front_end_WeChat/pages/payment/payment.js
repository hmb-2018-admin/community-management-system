Page({
  data: {
    orderList:[],               // 缴费列表
    hasList:false,          // 列表是否有数据
    totalPrice:0,           // 总价，初始为0
    selectAllStatus:false,    // 全选状态，默认全选
  },
  onShow() {
    this.setData({
      hasList: true,
      orderList:[
        { id:1,name: '电费',liang:"用电量：",value:"78度",time:"03/12-04/12",price:78.02,selected:false},
        { id:2,name: '水费',liang:"用水量：",value:"3吨",time:"03/12-04/12",price:78.52,selected:false},
        { id:3,name: '物管费',time:"03/12-04/12",price:178.00,selected:false},
        { id:4,name: '车位管理费',time:"03/12-04/12",price:58.42,selected:false},
        { id:5,name: '燃气费',liang:"用水量：",value:"45",time:"03/12-04/12",price:110.02,selected:false}
      ]
    });
    this.getTotalPrice();
  },
  /**
   * 当前商品选中事件
   */
  selectList(e) {
    const index = e.currentTarget.dataset.index;
    let orderList = this.data.orderList;
    const selected = orderList[index].selected;
    orderList[index].selected = !selected;
    this.setData({
      orderList: orderList
    });
    this.getTotalPrice();
  },


  /**
   * 全选
   */
  selectAll(e) {
    let selectAllStatus = this.data.selectAllStatus;
    selectAllStatus = !selectAllStatus;
    let orderList = this.data.orderList;

    for (let i = 0; i < orderList.length; i++) {
      orderList[i].selected = selectAllStatus;
    }
    this.setData({
      selectAllStatus: selectAllStatus,
      orderList: orderList
    });
    this.getTotalPrice();
  },
    /**
   * 计算总价
   */
  getTotalPrice() {
    let orderList = this.data.orderList;                  // 获取购物车列表
    let total = 0;
    for(let i = 0; i<orderList.length; i++) {         // 循环列表得到每个数据
      if(orderList[i].selected) {                     // 判断选中才会计算价格
        total +=  orderList[i].price;   // 所有价格加起来
      }
    }
    this.setData({                                // 最后赋值到data中渲染到页面
      orderList: orderList,
      totalPrice: total.toFixed(2)
    });
  },

  toPay() {
    wx.showModal({
      title: '提示',
      content: '哎呀呀！没钱付款呀！',
      text:'center',
      complete() {
        wx.switchTab({
          url: '../index/index'
        })
      }
    })
  },
  record: function() {
    wx.navigateTo({
      url: '../payment/payment-record'
    })
  },
})