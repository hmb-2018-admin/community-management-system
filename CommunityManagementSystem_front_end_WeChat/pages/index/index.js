//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    state:false,
    first_click:false,
    userInfo:[],
  },

  //动画
  toggle: function(){
    var list_state = this.data.state,
        first_state = this.data.first_click;
    if (!first_state){
        this.setData({
          first_click: true
        });
    }
    if (list_state){
        this.setData({
          state: false
        });
    }else{
        this.setData({
          state: true
        });
    }
  },

  //前门开门方法
  openDoor:function(e){
    var doorname = e.currentTarget.dataset.doorname
    console.log("传参:",doorname);
    wx.request({
      url:app.globalData.baseUrl + '/public/open?doorname' + doorname,
      header:{
        'Content-Type': 'application/json',
        'Authorization': app.globalData.Authorization
      },
      method: "GET",
      success: function (res) {          
        console.log("kaimen:",res)
        if(res.data.code == 101){
          wx.showToast({
            title: '开门成功',
            duration: 2000,
          })
        }else{
          wx.showToast({
            title: '开门失败',
            duration:2000,
            icon:"none",
          })
        }
        }
    })
  },


  //事件处理函数
  payment: function() {
    wx.navigateTo({
      url: '../payment/payment'
    })
  },
  zhuangxiu: function() {
    wx.navigateTo({
      url: '../zhuangxiu/zhuangxiu'
    })
  },
  visiting: function() {
    wx.navigateTo({
      url: '../visit/visit'
    })
  },
  chewei: function() {
    wx.navigateTo({
      url: '../chewei/chewei'
    })
  },
  repair: function() {
    wx.navigateTo({
      url: '../repair/repair'
    })
  },
  gonggao: function() {
    wx.navigateTo({
      url: '../gonggao/gonggao'
    })
  },
  service: function() {
    wx.navigateTo({
      url: '../service/service'
    })
  },
  vote: function() {
    wx.navigateTo({
      url: '../vote/vote'
    })
  },

  onLoad: function () {
  //   if (app.globalData.userInfo) {
  //     this.setData({
  //       userInfo: app.globalData.userInfo,
  //       hasUserInfo: true
  //     })
  //   } else if (this.data.canIUse){
  //     // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
  //     // 所以此处加入 callback 以防止这种情况
  //     app.userInfoReadyCallback = res => {
  //       this.setData({
  //         userInfo: res.userInfo,
  //         hasUserInfo: true
  //       })
  //     }
  //   } else {
  //     // 在没有 open-type=getUserInfo 版本的兼容处理
  //     wx.getUserInfo({
  //       success: res => {
  //         app.globalData.userInfo = res.userInfo
  //         this.setData({
  //           userInfo: res.userInfo,
  //           hasUserInfo: true
  //         })
  //       }
  //     })
  //   }
  // },
  // getUserInfo: function(e) {
  //   console.log(e)
  //   app.globalData.userInfo = e.detail.userInfo
  //   this.setData({
  //     userInfo: e.detail.userInfo,
  //     // hasUserInfo: true
  //   })
  },
  onShow:function(){
     //用户昵称头像
     this.setData({
      userInfo :app.globalData.userInfo
    })
  }
})
