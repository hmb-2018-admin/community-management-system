// pages/mine/updateTel.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tel: ""
  },

  formSubmit: function (e) {
    console.log('tel', e.detail.value)
    var tel = e.detail.value;
    if (!(/^1[345789]\d{9}$/.test(e.detail.value.tel))) {
      wx.showToast({
        title: '手机号码有误！',
        image:"../../icon/fail.png"
      })
    }else{
    wx.request({
      url: app.globalData.baseUrl + '/proprietor/updateTel',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'Authorization': app.globalData.Authorization
      },
      method: "POST",
      data: tel,
      success: function (res) {
        console.log("baocun:",res)
        wx.showToast({
          title: '保存成功',
          duration: 2000,
          success: function (res) {
            setTimeout(function () { //延迟跳转
              wx.navigateBack({
                url: '../mine/my-information'
              })
            }, 2000)
          }
        })
        
      }
    })
  }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    wx.request({
      url: app.globalData.baseUrl + '/proprietor/proprietor',
      header: {
        'content-type': 'application/json',
        'Authorization': app.globalData.Authorization
      },
      method: "GET",
      success: function (res) {
        console.log("getTel:",res.data)
       that.setData({
          tel: res.data.data.tel
        })
      }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})