// pages/mine/mine.js
const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
      message:[],
      userInfo:[]
    },

    untie:function(){
      wx.showModal({
        title:"提示",
        content: "是否确定解绑",
        success (res) {
          if (res.confirm) {
            console.log('用户点击确定')
            wx.request({
              url: app.globalData.baseUrl + '/public/untie',
              header: {
                'content-type': 'application/x-www-form-urlencoded',
                'Authorization': app.globalData.Authorization
              },
              method: "POST",
              data:{
                role:0
              },
              success: function (res) {          
              console.log(res)
              if(res.data.code == 101){
                wx.showToast({
                  title: '解绑成功',
                  duration: 2000,
                  success: function (res) {
                    setTimeout(function () { //延迟跳转
                      wx.redirectTo({
                        url: '../login/login',
                      })
                    }, 2000)
                  }
                })
              }else{
                wx.showToast({
                  title: '解绑失败',
                  image:"../../icon/fail.png"
                })
              }
              }
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
      
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {   
     
},

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {     
      //用户昵称头像
      this.setData({
        userInfo :app.globalData.userInfo
      })
      // //检查登录态
      // wx.checkSession({
      //   success: (res) => {
      //     return;
      //   },
      //   fail: (res) =>{
      //     wx.login({
      //       success: function (res) {
      //         console.log("生成code成功:", res)
      //         let {code} = res
      //         //发送请求
      //         wx.request({
      //           url: app.globalData.baseUrl + '/auth/weChatLogin', //接口地址
      //           data: {
      //             code: code
      //           },
      //           method: 'POST',
      //           header: {
      //             'content-type': 'application/x-www-form-urlencoded', 
      //             'Authorization': app.globalData.Authorization       
      //           },
      //           success: function (res) {
      //             console.log("res:", res.data)
      //             let token = res.data.data.token
      //             app.globalData.Authorization = token
      //             if(res.data.msg == "成功"){
      //               //是否已经绑定信息
      //               if (res.data.data.isCompleted == 1) {  
      //               console.log("res:", res.data)   
      //               wx.switchTab({
      //                 url: '../index/index',
      //               })   
      //             }
      //           }else{
      //             wx.showToast({
      //                   title: '登陆失败',
      //                   image:"../../icon/fail.png",
      //                 })
      //           }
      //           }
      //         })
      //       }
      //     })
      //   }
      // })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})