const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    userInfo: ""
  },

  /**
     * 生命周期函数--监听页面显示
     */
  onShow: function () {
    //隐藏左上角返回键
    wx.hideHomeButton({
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    var that = this
    // 查看是否授权
    wx.getSetting({
      success: function (res) {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserProfile({
            success: function (res) {
              console.log("用户信息：", res)
              app.globalData.userInfo = res.userInfo
              // 已授权
              that.setData({
                userInfo: res.userInfo
              })
              //缓存用户信息
              wx.setStorageSync('userInfo', res.userInfo)
              // 授权成功后
              wx.login({
                success: function (res) {
                  console.log("生成code成功:", res)
                  let { code } = res
                  //发送请求
                  wx.request({
                    url: app.globalData.baseUrl + '/auth/weChatLogin', //接口地址
                    data: {
                      code: code
                    },
                    
                    method: 'POST',
                    header: {
                      'content-type': 'application/x-www-form-urlencoded',
                      'Authorization': app.globalData.Authorization
                    },
                    
                    success: function (res) {
                      console.log("已经授权,执行登陆:", res.data)
                      let token = res.data.data.token
                      console.log("token:", token)
                      app.globalData.Authorization = token
                      if (res.data.code == 101) {
                        //isCompleted == 1 已经完善信息直接跳转
                        if (res.data.data.isCompleted == 1) {
                          //role == 0 身份为业主
                          if (res.data.data.roleFlag == 0) {
                            wx.switchTab({
                              url: '../index/index',
                            })
                          } else {
                            //保安
                            wx.navigateTo({
                              url: '../guard/index/index',
                            })
                          }
                        } else {
                          //未绑定信息，跳转信息绑定页面
                          wx.showToast({
                            title: '加载中',
                            icon:"loading",
                            success: function (res) {
                              setTimeout(function () { //延迟跳转
                                wx.navigateTo({
                                  url: '../login/login-information',
                                })
                              }, 1500)
                            }
                          })
                         
                        }
                      } else {
                        //登陆失败的情况
                        wx.showModal({
                          title: '提示',
                          content: res.data.msg,
                          showCancel: false,
                          confirmText: '好的',
                        })
                      }
                    }
                  })
                }
              })
            }
          })
        } else {
          // 没有授权
        }
      }
    })
  },

  // 请求API授权，获得用户头像和昵称
  getuserinfo: function (e) {
    console.log("lll",e)
    if (e.detail.userInfo) {
      var that = this;
      console.log("点击授权按钮：", e.detail.userInfo);
      var userInfo = e.detail.userInfo
      wx.setStorageSync('userInfo', userInfo)
      app.globalData.userInfo = e.detail.userInfo
      that.setData({
        userInfo: userInfo
      });
      //点击授权成功，执行登录
      wx.login({
        success: function (res) {
          console.log("生成code成功:", res)
          let { code } = res
          wx.request({
            url: app.globalData.baseUrl + '/auth/weChatLogin', //接口地址
            data: {
              code: code
            },
            method: 'POST',
            header: {
              'content-type': 'application/x-www-form-urlencoded',
              'Authorization': app.globalData.Authorization
            },
            success: function (res) {
              console.log("点击授权登陆成功:", res.data)
              let token = res.data.data.token
              app.globalData.Authorization = token
              if (res.data.code == 101) {
                //是否已经绑定信息
                console.log("isCompleted:", res.data.data.isCompleted)
                if (res.data.data.isCompleted == 1) {
                  if (res.data.data.roleFlag == 0) {
                    wx.switchTab({
                      url: '../index/index',
                    })
                  } else {
                    wx.navigateTo({
                      url: '../guard/index/index',
                    })
                  }
                } else {
                  wx.navigateTo({
                    url: '../login/login-information',
                  })
                 
                }
              } else {
                wx.showModal({
                  title: '提示',
                  content: res.data.msg,
                  showCancel: false,
                  confirmText: '好的',
                })
              }
            }
          })
        }
      })
    } else {
      //拒绝授权
      wx.showModal({
        title: '警告',
        content: '点击拒绝授权，将无法进入小程序，请返回重新授权!',
        showCancel: false,
        confirmText: '返回授权',
        success: function (res) {
          // if (res.confirm) {
          //   console.log('返回授权');
          // }
        }
      });
    }
  },

})