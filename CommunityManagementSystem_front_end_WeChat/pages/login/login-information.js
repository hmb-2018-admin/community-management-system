// pages/login/login-information.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: "",
    radioId: "",
    role: [
      { id: 0, name: "业主" },
      { id: 1, name: "保安" },
    ],
  },


  //单选
  updataRadio: function (e) {
    var Id = e.detail.value.id
    this.setData({
      radioId: Id
    })
  },

  /**
   * 提交完善信息
   */
  formSubmit: function (e) {
    console.log('填写完善信息的数据', e.detail.value)
    var informationData = e.detail.value;
    if (!(/^[1-9]\d{5}(18|19|20|(3\d))\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/.test(e.detail.value.idCard))) {
      wx.showToast({
        title: '身份证号码有误！',
        image:"../../icon/fail.png"
      })
    }
    else if (!(/^1[345789]\d{9}$/.test(e.detail.value.tel))) {
      wx.showToast({
        title: '手机号码有误',
        image:"../../icon/fail.png"
      })
    }
    else {
      wx.request({
        url: app.globalData.baseUrl + '/public/completeInfo',//接口地址     
        method: "POST",
        data: informationData,
        header: {
          'content-type': 'application/x-www-form-urlencoded',
          'Authorization': app.globalData.Authorization
        },
        success: function (res) {
          console.log("绑定信息返回数据：", res.data)
          if (res.data.code == 101) {
            //如果role为0，即用户身份是业主，跳转到首页
            if (res.data.data.role == 0) {
              wx.showToast({
                title: '提交成功',
                duration: 2000,
                success: function (res) {
                  setTimeout(function () { //延迟跳转
                    wx.switchTab({
                      url: '../index/index'
                    })
                  }, 2000)
                }
              })
            } else {    //身份为保安
              wx.showToast({
                title: '提交成功',
                duration: 2000,
                success: function (res) {
                  setTimeout(function () { //延迟跳转
                    wx.navigateTo({
                      url: '../guard/index/index'
                    })
                  }, 2000)
                }
              })
            }
          } else {
            wx.showModal({
              title: "提示",
              content: res.data.msg,
              showCancel: false,
              confirmText: '好的',
            })
          }
        }
      })
    }
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userInfo: app.globalData.userInfo
    })
    // wx.hideHomeButton({
    // })
  },

})