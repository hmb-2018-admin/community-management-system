// pages/chewei/chewei.js
const app = getApp()

Page({
  data: { 
    hasPacking:false,
    licenses:[]
  },
  //bindinput
  getCarLicense:function(e){   
    var index = e.currentTarget.dataset.index;
    var  licenses = e.detail.value
    // debugger;
    var arr = "licenses[" + index + "].carLicense"
    this.setData({
      [arr]:licenses
    })
    // console.log("license:",this.data.licenses)
  },

  //保存修改
  save:function(){
    console.log("修改后数据",this.data.licenses)
    var list = []
    var licen = this.data.licenses
    for(var i = 0 ;i <licen.length; i++)  {
      list.push(licen[i].carLicense)
    }
    console.log("new",list)
    wx.request({
      url: app.globalData.baseUrl + '/proprietor/license',
      method:"POST",
      data:
      {
        licenses:list
      },
      header: {         
        'content-type': 'application/json',
        'Authorization': app.globalData.Authorization
     },    
   
      success: function (res) {  
        console.log("res:",res)
        if(res.data.code == 101){
          wx.showToast({
            title: '保存成功',
            success:function(){
            setTimeout(function () { 
              wx.switchTab({
                url: '../index/index',
              })
            }, 2000)
          }
          })
          
        }else{
          wx.showToast({
            title: '保存失败',
            image:"../../icon/fail.png"
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    wx.request({
      url: app.globalData.baseUrl + '/proprietor/getLicense', 
      method:'GET',
      header:{
        'content-type': 'application/json',
        'Authorization': app.globalData.Authorization
      } ,
            success: function (res) {
            console.log("data:",res.data.data)
            if(res.data.data.num == 0){
              that.setData({
                hasPacking:false
              })
            }else{
              that.setData({
                hasPacking:true,
                licenses:res.data.data.licenses
              })
            }
            }
      })

  },

 

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})