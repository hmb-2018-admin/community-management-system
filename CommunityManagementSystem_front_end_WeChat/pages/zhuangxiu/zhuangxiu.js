// pages/zhuangxiu/zhuangxiu.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    img_arr: []
  },

  //图片选择
  upimg: function () {
    var that = this;
    wx.chooseImage({
      // count: 9,    //默认为9
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        that.setData({
          img_arr: that.data.img_arr.concat(res.tempFilePaths)
        })
        
      }
    })
  },

  // 图片预览
  previewImg: function (e) {
    //获取当前图片的下标
    var index = e.currentTarget.dataset.index;
    var imgs = this.data.img_arr;
    wx.previewImage({
      //显示图片
      current: imgs[index],
      urls: imgs
    })
  },
  // 删除图片
  deleteImg: function (e) {
    var imgs = this.data.img_arr;
    var index = e.currentTarget.dataset.index;
    imgs.splice(index, 1);
    this.setData({
      imgs: imgs
    });
  },

  //图片上传
  upload: function () {
    var that = this
    for (var i = 0; i < that.data.img_arr.length; i++) {
      wx.uploadFile({
        url: app.globalData.baseUrl + '/proprietor/decoration',
        header: {
          'content-type': 'multipart/form-data',
          'Authorization': app.globalData.Authorization
        },
        filePath: that.data.img_arr[i],
        name: 'pic',
        success: function (res) {
          console.log("提交状态:", res)
          //字符串转换
          var data = JSON.parse(res.data)
          console.log("data:",data)
          if (data.code == 101) {
            wx.showToast({
              title: '提交成功',
              duration: 2000,
              success: function (res) {
                setTimeout(function () { //延迟跳转
                  wx.switchTab({
                    url: '../index/index'
                  })
                }, 2000)
              }
            })
          }
        }
      })
    }
  },

})