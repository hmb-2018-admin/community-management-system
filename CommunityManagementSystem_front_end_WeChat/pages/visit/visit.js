const app=getApp()
Page({
    /**
     * 页面的初始数据
     */
    data: {
       //来访人数
       visitor: ['1', '2', '3', '4','5', '6', '7', '8','9', '10', '11', '12','13', '14', '15', '16','17', '18', '19', '20'],
       vIndex:0,//从下标为0的开始
       //来访天数
       day: ['1', '2', '3', '4','5', '6', '7', '8','9', '10'],    
       dIndex:0,  
      //  sub_arr:[],
      /**
       * 车牌号输入键盘数据
       */
      provinces: [
        ['京', '沪', '粤', '津', '冀', '晋', '蒙', '辽', '吉', '黑'],
        ['苏', '浙', '皖', '闽', '赣', '鲁', '豫', '鄂', '湘'],
        ['桂', '琼', '渝', '川', '贵', '云', '藏'],
        ['陕', '甘', '青', '宁', '新'],
      ],
      numbers: [
        ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
        ["A", "B", "C", "D", "E", "F", "G", "H", "J", "K"],
        ["L", "M", "N", "P", "Q", "R", "S", "T", "U", "V"],
        ["W", "X", "Y", "Z", "港", "澳", "学"]
      ],
      carnum: [],
      showNewPower: false,
      KeyboardState: false
    },
    /**
     * 来访人数
     */
    bindPickerChangeVisitor: function(e) {
      console.log('来访人数为：', this.data.visitor[e.detail.value])
      this.setData({
        vIndex: e.detail.value,
      })
    },
    /**
     * 来访天数
     */
    bindPickerChangeDay: function(e) {
      console.log('来访天数为：', this.data.day[e.detail.value])
      this.setData({
        dIndex: e.detail.value,
      })
    },

    //来访时间
    bindDateChange: function (e) {
        console.log('来访时间为：', e.detail.value)
        this.setData({
          date: e.detail.value
        })
      },
    // 键盘
    bindChoose(e) {
      if (!this.data.carnum[6] || this.data.showNewPower) {
        var arr = [];
        arr[0] = e.target.dataset.val;
        this.data.carnum = this.data.carnum.concat(arr)
        this.setData({
          carnum: this.data.carnum
        })
      }
    },
    /**
     *  删除键
     */
    bindDelChoose() {
      if (this.data.carnum.length != 0) {
        this.data.carnum.splice(this.data.carnum.length - 1, 1);
        this.setData({
          carnum: this.data.carnum
        })
      }
    },
    /**
     * 收起键盘
     */
    closeKeyboard() {
      this.setData({
        KeyboardState: false
      })
    },
    /**
     * 弹出键盘
     */
    openKeyboard() {
      this.setData({
        KeyboardState: true
      })
    },
    
   

    /**
     * 表单提交数据
     */
    formSubmit: function (e) {    
      var visitData = e.detail.value;
      console.log('获得数据：', visitData)
      if (!(/^1[3|4|5|7|8|9][0-9]{9}$/.test(e.detail.value.visitorTel)) 
      || e.detail.value.visitorTel == ""){     
        wx.showToast({   
        title: '请输入正确的手机号码！',
        icon:'none'    
        }) 
      } 
      else if(e.detail.value.roomId == "" ){
        wx.showToast({   
          title: '请输入房间号',
          icon:'none'    
          }) 
      }
      else if(e.detail.value.visitorTime == " " ){
        wx.showToast({   
          title: '请选择来访时间！',
          icon:'none'    
          }) 
      }
      else {  
        wx.request({   
          url: app.globalData.baseUrl+'/proprietor/visitor',//接口地址     
          method:"POST",
          data:visitData,
          header: {         
            'content-type': 'application/x-www-form-urlencoded',
            'Authorization': app.globalData.Authorization
         },      
          success: function (res) {  
            console.log("提交来访申请返回信息：",res) 
            app.globalData.passCode = res.data.data.passCode 
            console.log("passCode:",app.globalData.passCode)
            if(res.data.code == 101)  {
              wx.showToast({
                title: '提交成功',
                duration: 2000,
                success: function (res) {
                  setTimeout(function () { //延迟跳转
                    wx.redirectTo({
                      url: '../visit/opendetails',
                    })
                  }, 2000)
                }
              })
            // wx.showModal({
            //   title: "提交成功",
            //   content: "请点击来访记录查看通行码！",
            //   showCancel: false,
            //   confirmText: '好的',
            // })
          }else{
            wx.showModal({
              title: '失败',
              content: '提交失败，请稍后再试！',
              showCancel: false,
              confirmText: '确定',
              success: function (res) {
                if (res.confirm) {
                  console.log('重新提交');
                }
              }
            });
          }
          },
          fail:function(res){
            wx.showModal({
              title: '警告',
              content: '提交失败，请重新提交！!',
              showCancel: false,
              confirmText: '确定',
              success: function (res) {
                if (res.confirm) {
                  console.log('重新提交');
                }
              }
            });
          }
        })
      } 
    },

    /**
     * 跳转来访记录
     */
    record:function(res){
      wx.navigateTo({
        url: '../visit/visit-record',
      })
    },

    
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})