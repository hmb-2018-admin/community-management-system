// pages/zhuangxiu/zhuangxiu.js
const app = getApp()
Page({
  data: {
    userInfo:[],
    num: "2",
    state: false,
    first_click: false,
    message:[],
  },

  // //扫描二维码
  // scancode: function () {
  //   var that = this;
  //   // var show;
  //   // 允许从相机和相册扫码
  //   wx.scanCode({
  //     // success(res) {
  //     // console.log(res);
  //     success: (res) => {
  //       console.log("扫描结果：",res);
  //       this.show = res.result;
  //       this.pdd = 'ture';
  //       that.setData({
  //         show: this.show,
  //         pdd: this.pdd,
  //       })
  //     },
  //     fail: (res) => {
  //       console.log(res);
  //       wx.showToast({
  //         title: '扫描失败',
  //         icon: 'none',
  //         duration: 2000
  //       })
  //     }
  //   })
  // },
  /**
   * 页面的初始数据
   */

  unbundling:function(){
    wx.request({
      url: app.globalData.baseUrl + '/public/untie',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'Authorization': app.globalData.Authorization
      },
      method: "POST",
      data:{
        role:1
      },
      success: function (res) {     
      if(res.data.code == 101){
        wx.showToast({
          title: '解绑成功',
          duration: 2000,
          success: function (res) {
            setTimeout(function () { //延迟跳转
              wx.reLaunch({
                url: '../../untie/untie'
              })
            }, 2000)
          }
        })
      }else{
        wx.showToast({
          title: '解绑失败',
          image:"/icon/fail.png"
        })
      }
      }
    })
  },

  repair: function() {
    wx.navigateTo({
      url: '../repair/repair'
    })
  },

  scanning:function(){
    wx.navigateTo({
      url: '../scanning/scanning'
    })
  },
  // 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      userInfo :app.globalData.userInfo
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})