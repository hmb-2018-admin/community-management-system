const app = getApp()
var adds = {};
Page({

  /**
   * 页面的初始数据
   */
  data: {
    img_arr: [],
    formdata: '',

  },
  formSubmit: function (e) {   
    adds = e.detail.value;
    this.upload()
  },
  upload: function () {
    var that = this
    for (var i = 0; i < that.data.img_arr.length; i++) {
      wx.uploadFile({
        url: app.globalData.baseUrl + '/public/repair',
        method:"POST",
        header: {
          'content-type': 'multipart/form-data',
          'Authorization': app.globalData.Authorization
        },
        filePath: that.data.img_arr[i],
        name: 'pic',
        formData: adds,
        success: function (res) {
          console.log("提交状态:", res.data)
          console.log("提交状态:", res)
          var data = JSON.parse(res.data)
          console.log("data:",data)
          if (data.code == 101) {
            wx.showToast({
              title: '提交成功',
              duration: 2000,
              success: function (res) {
                setTimeout(function () { //延迟跳转
                  wx.reLaunch({
                    url: '/pages/guard/index/index', 
                    })  
                    }, 2000);  
                    }
            })
          }else{
            wx.showToast({
              title: '提交失败',
              image:"../../icon/fail.png"
            })
          }
        }
      })
    }
    this.setData({
      formdata: ''
    })
  },
  upimg: function () {
    var that = this;
    // console.log("lenh:",this.data.img_arr.length)
    // var length =this.data.img_arr.length
    // if (this.data.img_arr.length  < 3) {
      wx.chooseImage({
        // count: 3,
        sizeType: ['original', 'compressed'],
        sourceType: ['album', 'camera'],
        success: function (res) {
          // console.log("lenh:",length)
          that.setData({
            img_arr: that.data.img_arr.concat(res.tempFilePaths)
          })
        }
      })
    // } else {
    //   wx.showToast({
    //     title: '最多上传三张图片',
    //     image:"../../icon/fail.png"
    //   });
    // }
  },
    // 预览图片
    previewImg: function (e) {
      //获取当前图片的下标
      var index = e.currentTarget.dataset.idx;
      var imgs = this.data.img_arr;
      wx.previewImage({
        //显示图片
        current: imgs[index],
        urls: imgs
      })
    },
  // 删除图片
  deleteImg: function (e) {
    var imgs = this.data.img_arr;
    var index = e.currentTarget.dataset.index;
    imgs.splice(index, 1);
    this.setData({
      imgs: imgs
    });
  },


  //查看报修记录
  record: function () {
    wx.navigateTo({
      url: '../repair/repair-record',
    })
  },

  // //报修上传图片
  // uploadimg: function () {
  //   var that = this;
  //   //选择图片
  //   wx.chooseImage({
  //     count: 9,
  //     sizeType: ['original', 'compressed'],
  //     sourceType: ['album', 'camera'],
  //     success(res) {
  //       // tempFilePath可以作为img标签的src属性显示图片
  //       var tempFilePaths = res.tempFilePaths
  //       that.setData({
  //         imgs: tempFilePaths
  //       })
  //       for (var i = 0; i < that.data.imgs.length; i++) {
  //         console.log("img_url:", that.data.imgs[i])
  //       }
  //       //上传
  //       wx.uploadFile({
  //         url: app.globalData.baseUrl+ '/public/repair', //接口地址
  //         filePath: tempFilePaths[i],
  //         name: 'file',
  //         header: {
  //           'content-type': 'multipart/form-data',
  //           'Authorization': app.globalData.Authorization
  //         },
  //         formData: null,
  //         success: function (res) {
  //           console.log(res) //接口返回网络路径
  //           // var data = JSON.parse(res.data)
  //           //   that.data.picPaths.push(data['msg'])
  //           //   that.setData({
  //           //     picPaths: that.data.picPaths
  //           //   })
  //           //   console.log(that.data.picPaths)
  //         }
  //       })
  //     }
  //   })
  // },





  // //提交数据
  // formSubmit: function (e) {
  //   var that = this;
  //   console.log('提交数据为：', e.detail.value)
  //   var repairData = e.detail.value;                    
  //   if (e.detail.value.location == '' || e.detail.value.repairSituation == '') {
  //     wx.showToast({
  //       title: '请把信息填写完整···',
  //       icon: 'none'
  //     })
  //   } else {
  //     wx.request({
  //       url:app.globalData.baseUrl+ '/public/repair',//接口地址     
  //       method: "POST",
  //       data: repairData,
  //       header: {
  //         'content-type': 'application/x-www-form-urlencoded',
  //         'Authorization': app.globalData.Authorization
  //       },
  //       success: function (res) {
  //         console.log("123", res.data)
  //         wx.showToast({
  //           title: '提交成功',
  //           duration: 2000,
  //           success: function (res) {
  //             setTimeout(function () { //延迟跳转
  //               wx.switchTab({
  //                 url: '../index/index'         
  //               })          
  //             }, 2000)          
  //           }            
  //         })
  //         that.setData({
  //           form_info: ''
  //         })
  //       }
  //     })
  //   }
  // },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})