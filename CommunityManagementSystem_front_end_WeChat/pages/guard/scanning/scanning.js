// pages/zhuangxiu/zhuangxiu.js
const app = getApp()
Page({
  data: {
    userInfo:[],
    num: "2",
    state: false,
    first_click: false,
    show: "",
    pdd: '',
    wwk: '',
    code: '',
  },

  //扫描二维码
  scancode: function () {
    var that = this;
    // var show;
    // 允许从相机和相册扫码
    wx.scanCode({
      // success(res) {
      // console.log(res);
      success: (res) => {
        console.log("扫描结果：",res);
        this.show = res.result;
        this.pdd = 'ture';
        that.setData({
          show: this.show,
          pdd: this.pdd,
        })
      },
      fail: (res) => {
        console.log(res);
        wx.showToast({
          title: '扫描失败',
          icon: 'none',
          duration: 2000
        })
      }
    })
  },
  /**
   * 页面的初始数据
   */

  toggle: function () {
    var list_state = this.data.state,
      first_state = this.data.first_click;
    if (!first_state) {
      this.setData({
        first_click: true
      });
    }
    if (list_state) {
      this.setData({
        state: false
      });
    } else {
      this.setData({
        state: true
      });
    }
  },

  formSubmit: function (e) {
    var that = this;
    var passCode = this.data.show;
    var tel = e.detail.value.tel;
    console.log('提交数据为：', tel, '+', passCode)
    this.wwk = 'ture';
    that.setData({
      wwk: this.wwk,
    })
    if (e.detail.value.tel == '') {
      wx.showToast({
        title: '请把信息填写完整···',
        icon: 'none'
      })
    } else {
      wx.request({
        url:app.globalData.baseUrl +  '/guard/verification',//接口地址     
        method: "GET",
        data:{
          tel,
          passCode,
        },
        header: {
          'Content-Type': 'application/json',
          'Authorization': app.globalData.Authorization
        },
        success: function (res) {          
          console.log("提交后返回信息:",res.data)     
          // this.code = this.result,
            that.setData({
              form_info: '',
              code: res.data.code,
            })
            
            if(res.data.code==101){
              wx.showModal({  
                title: '提示',  
                content: '允许进入',  
                confirmText:"继续扫描", 
                cancelText:"返回首页",
                // confirmColor: 'skyblue',
                success: function(res) {  
             if (res.confirm) {  
                    //刷新页面
                  wx.showToast({
                    title: '刷新中',
                    icon: 'loading',
                    duration: 200,
                    success: function () {
                      setTimeout(function () {
                        wx.reLaunch({
                          url: '../../guard/scanning/scanning',
                        })
                      }, 200);
                    }
                  })  
                    } else if (res.cancel) {  
                      wx.showToast({
                        title: '刷新中',
                        icon: 'loading',
                        duration: 200,
                        success: function () {
                          setTimeout(function () {
                            wx.reLaunch({
                              url: '../../guard/index/index',
                            })
                          }, 200);
                        }
                      })    
                    }  
                }  
            })  
            }else{
              wx.showModal({  
                title: '提示',  
                content: '此通行码不匹配手机号或已过期，不可通行', 
                confirmText:"重新扫描", 
                cancelText:"返回重填",
                // cancelColor:'black',
                // confirmColor: 'black',
                success: function(res) {  
                    if (res.confirm) {  
                    //刷新页面
                      wx.showToast({
                        title: '刷新中',
                        icon: 'loading',
                        duration: 200,
                        success: function () {
                          setTimeout(function () {
                            wx.reLaunch({
                              url: '../../guard/scanning/scanning',
                            })
                          }, 200);
                        }
                      })  
                    } else if (res.cancel) {  
                    console.log('用户点击取消')  
                    }  
                }  
            })  
            }
        }
      })
    }
  },   


  // 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      userInfo :app.globalData.userInfo
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})