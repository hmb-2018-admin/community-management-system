/*
Navicat MySQL Data Transfer

Source Server         : 阿里云
Source Server Version : 50731
Source Host           : 39.98.110.225:3306
Source Database       : community

Target Server Type    : MYSQL
Target Server Version : 50731
File Encoding         : 65001

Date: 2021-01-10 16:18:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cost
-- ----------------------------
DROP TABLE IF EXISTS `cost`;
CREATE TABLE `cost` (
  `cost_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` varchar(255) DEFAULT NULL COMMENT '房号',
  `car_fee` varchar(255) DEFAULT NULL COMMENT '车位管理费',
  `pro_fee` varchar(255) DEFAULT NULL COMMENT '物管费',
  `el_fee` varchar(255) DEFAULT NULL COMMENT '电费',
  `el_degree` varchar(255) DEFAULT NULL COMMENT '电度数',
  `water_fee` varchar(255) NOT NULL COMMENT '水费',
  `water_tonnes` varchar(255) DEFAULT NULL COMMENT '水吨数',
  `gas_fee` varchar(255) DEFAULT NULL COMMENT '天然气费',
  `gas_vol` varchar(255) DEFAULT NULL COMMENT '天然气使用总数',
  `total` varchar(255) DEFAULT NULL COMMENT '总数',
  `c_time` varchar(255) DEFAULT NULL COMMENT '账单年月',
  `status` int(11) DEFAULT '0' COMMENT '是否已经缴费？：0未交费，1已缴费',
  PRIMARY KEY (`cost_id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COMMENT='费用表';

-- ----------------------------
-- Records of cost
-- ----------------------------
INSERT INTO `cost` VALUES ('55', '1', '100', '160', '200', '300', '250', '250', '242', '42', '952.0', '2020-01', '1');
INSERT INTO `cost` VALUES ('56', '11213', '100', '160', '201', '301', '250', '250', '243', '43', '954.0', '2020-02', '0');
INSERT INTO `cost` VALUES ('57', '12', '100', '160', '202', '302', '250', '250', '244', '44', '956.0', '2020-01', '0');
INSERT INTO `cost` VALUES ('58', '121', '100', '160', '203', '303', '250', '250', '245', '45', '958.0', '2020-02', '0');
INSERT INTO `cost` VALUES ('59', '1701', '100', '160', '204', '304', '250', '250', '246', '46', '960.0', '2020-03', '0');
INSERT INTO `cost` VALUES ('60', '1702', '100', '160', '205', '305', '250', '250', '247', '47', '962.0', '2020-04', '0');
INSERT INTO `cost` VALUES ('61', '520', '100', '160', '206', '306', '250', '250', '248', '48', '964.0', '2020-05', '0');
INSERT INTO `cost` VALUES ('62', '1', '10', '1', '1', '1', '1', '1', '1', '1', '14.0', '2020-01', '0');
INSERT INTO `cost` VALUES ('63', '11213', '11', '2', '2', '2', '2', '2', '2', '2', '19.0', '2020-01', '0');
INSERT INTO `cost` VALUES ('64', '12', '12', '3', '3', '3', '3', '3', '3', '3', '24.0', '2020-02', '0');
INSERT INTO `cost` VALUES ('65', '121', '13', '4', '4', '4', '4', '4', '4', '4', '29.0', '2020-02', '0');
INSERT INTO `cost` VALUES ('66', '1701', '14', '5', '5', '5', '5', '5', '5', '5', '34.0', '2020-02', '0');
INSERT INTO `cost` VALUES ('67', '1702', '15', '6', '6', '6', '6', '6', '6', '6', '39.0', '2020-02', '0');
INSERT INTO `cost` VALUES ('68', '520', '16', '7', '7', '7', '7', '7', '7', '7', '44.0', '2020-01', '0');
INSERT INTO `cost` VALUES ('69', '1', '10', '1', '1', '1', '1', '1', '1', '1', '14.0', '2020-01', '0');
INSERT INTO `cost` VALUES ('70', '11213', '11', '2', '2', '2', '2', '2', '2', '2', '19.0', '2020-01', '0');
INSERT INTO `cost` VALUES ('71', '12', '12', '3', '3', '3', '3', '3', '3', '3', '24.0', '2020-02', '0');
INSERT INTO `cost` VALUES ('72', '121', '13', '4', '4', '4', '4', '4', '4', '4', '29.0', '2020-02', '1');
INSERT INTO `cost` VALUES ('73', '1701', '14', '5', '5', '5', '5', '5', '5', '5', '34.0', '2020-02', '1');
INSERT INTO `cost` VALUES ('74', '1702', '15', '6', '6', '6', '6', '6', '6', '6', '39.0', '2020-02', '0');
INSERT INTO `cost` VALUES ('75', '520', '16', '7', '7', '7', '7', '7', '7', '7', '44.0', '2020-01', '0');

-- ----------------------------
-- Table structure for decoration
-- ----------------------------
DROP TABLE IF EXISTS `decoration`;
CREATE TABLE `decoration` (
  `did` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `solve` int(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`did`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of decoration
-- ----------------------------
INSERT INTO `decoration` VALUES ('36', '装修测试', '0', '12');
INSERT INTO `decoration` VALUES ('38', '装修测试', '1', '12');
INSERT INTO `decoration` VALUES ('39', '底射', '0', '12');
INSERT INTO `decoration` VALUES ('40', '测试', '0', '12');
INSERT INTO `decoration` VALUES ('41', '测试', '0', '12');
INSERT INTO `decoration` VALUES ('42', '测试', '0', '12');
INSERT INTO `decoration` VALUES ('43', '测试', '0', '12');
INSERT INTO `decoration` VALUES ('44', null, '0', null);
INSERT INTO `decoration` VALUES ('45', null, '0', null);
INSERT INTO `decoration` VALUES ('46', null, '0', null);
INSERT INTO `decoration` VALUES ('47', null, '0', null);
INSERT INTO `decoration` VALUES ('48', null, '0', null);
INSERT INTO `decoration` VALUES ('49', null, '0', null);
INSERT INTO `decoration` VALUES ('50', null, '0', null);
INSERT INTO `decoration` VALUES ('66', null, '0', '54');
INSERT INTO `decoration` VALUES ('67', null, '0', '54');
INSERT INTO `decoration` VALUES ('68', null, '0', '54');
INSERT INTO `decoration` VALUES ('69', null, '0', '54');

-- ----------------------------
-- Table structure for door
-- ----------------------------
DROP TABLE IF EXISTS `door`;
CREATE TABLE `door` (
  `door_id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) DEFAULT NULL COMMENT '拥有数量',
  `apply_situation` varchar(255) DEFAULT NULL COMMENT '申请情况',
  `apply_number` int(11) DEFAULT NULL COMMENT '申请数量',
  `slove` int(11) DEFAULT NULL COMMENT '解决情况',
  `room_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`door_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of door
-- ----------------------------

-- ----------------------------
-- Table structure for guard
-- ----------------------------
DROP TABLE IF EXISTS `guard`;
CREATE TABLE `guard` (
  `guard_id` int(11) NOT NULL COMMENT '保安id',
  `guard_name` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `guard_period` varchar(255) DEFAULT NULL,
  `id_card` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`guard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保安表';

-- ----------------------------
-- Records of guard
-- ----------------------------
INSERT INTO `guard` VALUES ('1', '21', '21', '21', '21', '21');
INSERT INTO `guard` VALUES ('2', '123', '13112811567', null, '441900200008114855', null);
INSERT INTO `guard` VALUES ('18', null, '19875602233', null, '440400200002290229', '54');

-- ----------------------------
-- Table structure for license
-- ----------------------------
DROP TABLE IF EXISTS `license`;
CREATE TABLE `license` (
  `lid` int(11) NOT NULL AUTO_INCREMENT,
  `car_license` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL COMMENT '归属人',
  PRIMARY KEY (`lid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of license
-- ----------------------------
INSERT INTO `license` VALUES ('1', '粤HLW225', '1');
INSERT INTO `license` VALUES ('2', '粤WLW225', '1');
INSERT INTO `license` VALUES ('3', '粤CQW225', '2');
INSERT INTO `license` VALUES ('4', '粤SQW225', '2');
INSERT INTO `license` VALUES ('9', '粤Q22331', '54');
INSERT INTO `license` VALUES ('10', '粤B77883', '54');

-- ----------------------------
-- Table structure for management
-- ----------------------------
DROP TABLE IF EXISTS `management`;
CREATE TABLE `management` (
  `mid` int(11) NOT NULL AUTO_INCREMENT COMMENT '物业编号',
  `man_name` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT '用户编号',
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of management
-- ----------------------------

-- ----------------------------
-- Table structure for parking_record
-- ----------------------------
DROP TABLE IF EXISTS `parking_record`;
CREATE TABLE `parking_record` (
  `rid` int(255) NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `license` varchar(255) DEFAULT NULL COMMENT '车牌号(直接填车牌号)',
  `pid` varchar(255) DEFAULT NULL COMMENT '车位号',
  `entry_time` varchar(255) DEFAULT NULL COMMENT '进入时间 ( 格式 : yyyy-MM-dd HH:mm:ss )',
  `departure_time` varchar(255) DEFAULT NULL,
  `fee` varchar(255) DEFAULT NULL COMMENT '停车费',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of parking_record
-- ----------------------------
INSERT INTO `parking_record` VALUES ('1', '123', '1', '2019-04-05 12:13:09', null, null);
INSERT INTO `parking_record` VALUES ('2', '456', '2', '2019-04-05 12:13:14', null, null);
INSERT INTO `parking_record` VALUES ('3', '123', '3', '1651', '1', '1');
INSERT INTO `parking_record` VALUES ('4', '粤HLCF12', 'Q1', '2020-10-29 18:41:41', '2020-10-29 18:52:33', '0');
INSERT INTO `parking_record` VALUES ('5', '粤HLCF12', 'Q1', '2020-10-29 18:53:24', '2020-10-29 18:53:59', '0');
INSERT INTO `parking_record` VALUES ('6', '粤HLCF12', 'Q1', '2020-10-29 19:04:38', '2020-10-29 22:15:10', '0');
INSERT INTO `parking_record` VALUES ('7', '粤HLCF12', 'Q1', '2020-10-29 22:15:24', '2020-10-29 22:19:56', '0');
INSERT INTO `parking_record` VALUES ('8', '粤HLCF12', 'Q3', '2020-10-29 22:20:04', '2020-11-03 20:25:09', '148.75');
INSERT INTO `parking_record` VALUES ('9', '粤A00000', 'p01', '2020-11-02 11:43:40', '2020-11-02 14:49:27', '6.0');
INSERT INTO `parking_record` VALUES ('11', '粤LCFDSZ', 'Q1', '2020-11-03 20:06:19', null, null);
INSERT INTO `parking_record` VALUES ('12', '晋U01234', 'p01', '2020-11-03 16:09:10', '2020-11-03 20:28:59', '12.5');
INSERT INTO `parking_record` VALUES ('14', '晋U01234', 'p01', '2020-11-03 20:29:06', null, null);

-- ----------------------------
-- Table structure for parking_space
-- ----------------------------
DROP TABLE IF EXISTS `parking_space`;
CREATE TABLE `parking_space` (
  `pid` varchar(255) NOT NULL COMMENT '车位号',
  `type` int(11) DEFAULT NULL COMMENT '车位类型：0临时车位，1可出售车位，2可出租车位',
  `status` int(11) DEFAULT '0' COMMENT '车位状态：0未停，1已停',
  `uid` int(11) DEFAULT '-1' COMMENT '归属人(此处填uid)，若无则为-1',
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of parking_space
-- ----------------------------
INSERT INTO `parking_space` VALUES ('p01', '0', '1', '-1');
INSERT INTO `parking_space` VALUES ('p02', '0', '1', '-1');
INSERT INTO `parking_space` VALUES ('p03', '0', '1', '-1');
INSERT INTO `parking_space` VALUES ('p04', '0', '1', '-1');
INSERT INTO `parking_space` VALUES ('p05', '0', '1', '-1');
INSERT INTO `parking_space` VALUES ('p06', '0', '1', '-1');
INSERT INTO `parking_space` VALUES ('p07', '1', '1', '-1');
INSERT INTO `parking_space` VALUES ('p08', '1', '1', '-1');
INSERT INTO `parking_space` VALUES ('p09', '1', '1', '-1');
INSERT INTO `parking_space` VALUES ('p10', '1', '1', '-1');
INSERT INTO `parking_space` VALUES ('p11', '1', '1', '-1');
INSERT INTO `parking_space` VALUES ('p12', '1', '1', '-1');
INSERT INTO `parking_space` VALUES ('p13', '2', '1', '-1');
INSERT INTO `parking_space` VALUES ('p14', '2', '1', '-1');
INSERT INTO `parking_space` VALUES ('p15', '2', '1', '-1');
INSERT INTO `parking_space` VALUES ('p16', '2', '1', '-1');
INSERT INTO `parking_space` VALUES ('p17', '2', '1', '-1');
INSERT INTO `parking_space` VALUES ('p18', '2', '1', '-1');
INSERT INTO `parking_space` VALUES ('p19', '2', '1', '-1');
INSERT INTO `parking_space` VALUES ('p20', '2', '1', '-1');
INSERT INTO `parking_space` VALUES ('Q1', '1', '1', '54');
INSERT INTO `parking_space` VALUES ('Q123', '0', '0', '-1');
INSERT INTO `parking_space` VALUES ('Q2', '1', '1', '1');
INSERT INTO `parking_space` VALUES ('Q3', '2', '1', '-1');
INSERT INTO `parking_space` VALUES ('Q4', '1', '1', '-1');
INSERT INTO `parking_space` VALUES ('Q777', '0', '0', '-1');
INSERT INTO `parking_space` VALUES ('Q778', '0', '0', '-1');
INSERT INTO `parking_space` VALUES ('Q779', '1', '0', '-1');
INSERT INTO `parking_space` VALUES ('Q780', '2', '1', '-1');
INSERT INTO `parking_space` VALUES ('Q781', '1', '1', '-1');

-- ----------------------------
-- Table structure for pictures
-- ----------------------------
DROP TABLE IF EXISTS `pictures`;
CREATE TABLE `pictures` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  `repair_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of pictures
-- ----------------------------
INSERT INTO `pictures` VALUES ('35', 'http://img.lengmaoking.com/a63c0e52-da18-4267-9d9c-2951b4bf5dcb.jpg', null, '78');
INSERT INTO `pictures` VALUES ('36', 'http://img.lengmaoking.com/1323d19f-b95f-4061-9e70-6873ce2fa823.jpg', null, '78');
INSERT INTO `pictures` VALUES ('37', 'http://img.lengmaoking.com/ae4309da-55e7-4898-873a-1b2fa8056d79.jpg', null, '79');
INSERT INTO `pictures` VALUES ('38', 'http://img.lengmaoking.com/62827402-b3a4-4139-b9f1-a35aabac7423.jpg', null, '79');
INSERT INTO `pictures` VALUES ('39', 'http://img.lengmaoking.com/a63c0e52-da18-4267-9d9c-2951b4bf5dcb.jpg', '1', null);
INSERT INTO `pictures` VALUES ('40', 'http://img.lengmaoking.com/a63c0e52-da18-4267-9d9c-2951b4bf5dcb.jpg', '1', null);
INSERT INTO `pictures` VALUES ('41', 'http://img.lengmaoking.com/d5a781db-f667-42a5-a47f-10441dd74536.jpg', null, '89');
INSERT INTO `pictures` VALUES ('42', 'http://img.lengmaoking.com/87324a08-c9ef-4d21-aa41-76c5cd1644d5.jpg', null, '89');
INSERT INTO `pictures` VALUES ('44', 'http://img.lengmaoking.com/94526e02-b75d-452f-883e-00a0997dc26b.jpg', '36', null);
INSERT INTO `pictures` VALUES ('45', 'http://img.lengmaoking.com/72e41b83-4a80-4f9b-b076-c94c3af1bd45.jpg', '38', null);
INSERT INTO `pictures` VALUES ('46', 'http://img.lengmaoking.com/23c2f926-7656-41f5-b305-e13d584be7f7.jpg', '38', null);
INSERT INTO `pictures` VALUES ('47', 'http://img.lengmaoking.com/7a4fc793-e018-4579-941b-781207bf12bc.jpg', '39', null);
INSERT INTO `pictures` VALUES ('48', 'http://img.lengmaoking.com/b8d41e9f-ba80-44e7-9763-dbc3a9f76ddc.jpg', '39', null);
INSERT INTO `pictures` VALUES ('49', 'http://img.lengmaoking.com/e48bf0e6-3e12-4dc9-afe2-f45a6719bb22.jpg', '40', null);
INSERT INTO `pictures` VALUES ('50', 'http://img.lengmaoking.com/e07e82ac-ff49-4046-89ca-64d86d8edb99.jpg', '40', null);
INSERT INTO `pictures` VALUES ('51', 'http://img.lengmaoking.com/7f2c84f4-54ae-4ce2-830a-fff7af7d8bc5.jpg', '41', null);
INSERT INTO `pictures` VALUES ('52', 'http://img.lengmaoking.com/60153e76-6d04-4d28-a4d2-4e98f0d8e1f6.jpg', '41', null);
INSERT INTO `pictures` VALUES ('53', 'http://img.lengmaoking.com/a0f332ec-67fd-48bc-a000-708b95c8020c.jpg', '42', null);
INSERT INTO `pictures` VALUES ('54', 'http://img.lengmaoking.com/19fe3a16-e93e-499f-a9eb-7893c636a2d6.jpg', '42', null);
INSERT INTO `pictures` VALUES ('55', 'http://img.lengmaoking.com/ee2a2755-1788-4615-ba04-1144cc1c04dc.jpg', '43', null);
INSERT INTO `pictures` VALUES ('56', 'http://img.lengmaoking.com/90d3ec6a-f807-419c-a9cb-3f9e119092cc.jpg', '43', null);
INSERT INTO `pictures` VALUES ('57', 'http://img.lengmaoking.com/2b6b5f8c-6bec-44f8-9d1e-99a5021d3dfd.jpg', null, '96');
INSERT INTO `pictures` VALUES ('58', 'http://img.lengmaoking.com/faa6a781-9db8-47e7-b01e-21dcd4b419bc.jpg', null, '96');

-- ----------------------------
-- Table structure for proprietor
-- ----------------------------
DROP TABLE IF EXISTS `proprietor`;
CREATE TABLE `proprietor` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` varchar(11) NOT NULL COMMENT '房号',
  `prop_name` varchar(255) DEFAULT NULL,
  `id_card` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `car_id` int(11) DEFAULT NULL,
  `car_type` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of proprietor
-- ----------------------------
INSERT INTO `proprietor` VALUES ('22', '11213', '毛泉', '121', '112112112', '121', '21122', '2');
INSERT INTO `proprietor` VALUES ('23', '121', '1123121', '1223232', '3', '123', '123', '3');
INSERT INTO `proprietor` VALUES ('24', 'H403', '小罗', '440222199911112222', '13888888888', '112', '121', '4');
INSERT INTO `proprietor` VALUES ('26', '1702', 'seven', '440400200002290229', '19875602211', null, null, null);
INSERT INTO `proprietor` VALUES ('28', '1701', 'LengMao', '441900200008114853', '17666224628', null, null, '39');
INSERT INTO `proprietor` VALUES ('30', '1703', 'yezhu', '441900200008114888', '17666224666', null, null, '39');

-- ----------------------------
-- Table structure for repair
-- ----------------------------
DROP TABLE IF EXISTS `repair`;
CREATE TABLE `repair` (
  `repair_id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(255) DEFAULT NULL,
  `repair_situation` varchar(255) DEFAULT NULL COMMENT '报修情况',
  `solve` int(11) DEFAULT NULL COMMENT '是否维修？：0未维修，1已维修',
  `time` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL COMMENT '报修人',
  PRIMARY KEY (`repair_id`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of repair
-- ----------------------------
INSERT INTO `repair` VALUES ('78', '爱浦京', '停电', '1', '2020-10-29 15:03:10', '1');
INSERT INTO `repair` VALUES ('79', '清华科技园', '停水', '1', '2020-10-29 10:44:30', '2');
INSERT INTO `repair` VALUES ('81', '清华科技园', '停水', '0', null, '2');
INSERT INTO `repair` VALUES ('82', '402', '204', '1', '2020-10-30 17:02:59', '3');
INSERT INTO `repair` VALUES ('129', 'H301', '灯烧了', '0', null, '54');
INSERT INTO `repair` VALUES ('130', 'H301', '电梯门故障', '0', null, '54');
INSERT INTO `repair` VALUES ('132', '烤肉店', '肉不好吃', '0', null, '39');
INSERT INTO `repair` VALUES ('133', 'H301', '走廊灯烧了', '1', null, '54');
INSERT INTO `repair` VALUES ('143', 'H301', '电梯故障', '0', null, '54');
INSERT INTO `repair` VALUES ('144', 'H301', '门坏了', '0', null, '54');
INSERT INTO `repair` VALUES ('145', '脑子', '抽了', '0', null, '39');
INSERT INTO `repair` VALUES ('146', '脑子', '抽了', '0', null, '39');
INSERT INTO `repair` VALUES ('147', '525', '差点就不出门', '0', null, '54');
INSERT INTO `repair` VALUES ('148', '124235435', '3253464', '0', null, '54');
INSERT INTO `repair` VALUES ('149', '111111', '11', '0', null, '39');
INSERT INTO `repair` VALUES ('150', '123', '111', '0', null, '39');
INSERT INTO `repair` VALUES ('151', '4层', '灯坏', '0', null, '39');
INSERT INTO `repair` VALUES ('152', '222', '222', '0', null, '39');
INSERT INTO `repair` VALUES ('153', '333', '333', '0', null, '39');
INSERT INTO `repair` VALUES ('154', '12345', '12345', '0', null, '39');
INSERT INTO `repair` VALUES ('155', '123', '345', '0', null, '39');
INSERT INTO `repair` VALUES ('156', '345', '345', '0', null, '39');
INSERT INTO `repair` VALUES ('157', '402', '照明灯损坏', '0', null, '54');
INSERT INTO `repair` VALUES ('158', 'a402', '照明灯不亮', '0', null, '54');
INSERT INTO `repair` VALUES ('159', '123', '123', '0', null, '39');
INSERT INTO `repair` VALUES ('160', '2333', '3222', '0', null, '39');
INSERT INTO `repair` VALUES ('161', '2333', '3222', '0', null, '39');
INSERT INTO `repair` VALUES ('162', '566', '788', '0', null, '39');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `isLocked` int(11) DEFAULT '0' COMMENT '是否锁定?：0为不锁定（默认），1为锁定',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'Tea', '$2a$10$nXdJNkSwp4s5JpdqBymzgux5mRgPuZPvzIbeY2jNeUSETVCYRA47q', 'ROLE_PROPRIETOR', null, '0');
INSERT INTO `user` VALUES ('5', 'admin', '$2a$10$oedOEtjqlCYG5FkvppQgvOePsXVF6lxKSYioBzyl4w7V0d4P9IeFC', 'ROLE_MANAGEMENT', null, '0');
INSERT INTO `user` VALUES ('8', 'Tea1', '$2a$10$.Xyan3U.T6.lVW/2Mzx08erS1QQizLge/oPfHLpE53V77b5YeV5Nu', 'ROLE_MANAGEMENT', null, '0');
INSERT INTO `user` VALUES ('9', 'xiaoluo1', '$2a$10$l5Y5VILIfFfiYBxKrI0UaexjvA5jHvJq02N3.gwAdp4fuzNFxU4Ee', 'ROLE_GUARD', null, '1');
INSERT INTO `user` VALUES ('11', 'tea2', '$2a$10$faD9B9LT1akX6pZhGEnnR.brTqYIqWSVzTuAHxX299WlMEwXcheGm', 'ROLE_MANAGEMENT', null, '0');
INSERT INTO `user` VALUES ('12', 'mao', '$2a$10$kR6VbGrild1AjVWKnpNqnOCryMPZMsXSMd5WQQXcPuYCsNouu6v3q', 'ROLE_MANAGEMENT', null, '0');
INSERT INTO `user` VALUES ('31', 'xiaoluo', '$2a$10$YW7.h0DDElu1Qx2OnJmtuOy6C2L34poiszVxmroeXrXILOWaXNc5e', 'ROLE_MANAGEMENT', null, '0');
INSERT INTO `user` VALUES ('34', 'test3', '$2a$10$mZOt5jdgLcNaPp40vKHmL.N3Eh5jQ3UZfXN3r/1Fz3JOWH1BM1ina', 'ROLE_MANAGEMENT', null, '0');
INSERT INTO `user` VALUES ('36', 'abc1', '$2a$10$BHfBlZxlTKSljR.s/1VPNOF85J.JM//JUmOF9Ili62R.nzjhyHdZq', 'ROLE_GUARD', null, '0');
INSERT INTO `user` VALUES ('37', 'asas1', '$2a$10$aPyk9D67yP5Mq/3kaO.9F.KK6BIZPczh4RuttyCJNlaHQ17nqDc0u', 'ROLE_GUARD', null, '1');
INSERT INTO `user` VALUES ('39', ':-)_mini', null, 'ROLE_PROPRIETOR', 'oz13l5LTDmtwIXcwTGFIcK3QjrZs', '0');
INSERT INTO `user` VALUES ('52', 'Luo_mini', null, 'ROLE_PROPRIETOR', 'oz13l5E-VkDzN4e2gVxctXMe_fKQ', '0');
INSERT INTO `user` VALUES ('54', 'Sophie_mini', null, null, 'oz13l5ETKrSRH2_l5AhvkibbCRNI', '0');

-- ----------------------------
-- Table structure for visitor
-- ----------------------------
DROP TABLE IF EXISTS `visitor`;
CREATE TABLE `visitor` (
  `visit_id` int(11) NOT NULL AUTO_INCREMENT,
  `visitor_number` int(11) DEFAULT NULL COMMENT '来访人数',
  `reason` varchar(255) DEFAULT NULL COMMENT '来访原因',
  `visitor_name` varchar(255) DEFAULT NULL COMMENT '来访客人姓名',
  `visitor_tel` varchar(255) DEFAULT NULL COMMENT '访客联系信息',
  `room_id` varchar(255) DEFAULT NULL COMMENT '访问房号',
  `visit_time` varchar(255) DEFAULT NULL COMMENT '来访时间',
  `days` int(11) DEFAULT NULL COMMENT '来访天数',
  `temp_license` varchar(255) DEFAULT NULL COMMENT '临时车牌',
  `uuid` varchar(255) DEFAULT NULL COMMENT '随机码',
  `uid` int(11) NOT NULL COMMENT '申请人id',
  PRIMARY KEY (`visit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COMMENT='访客表';

-- ----------------------------
-- Records of visitor
-- ----------------------------
INSERT INTO `visitor` VALUES ('33', '1', '来访朋友', '李瑞峥', '13326682484', '507', '2020-10-27', '1', null, 'a640cd7e-f936-4d32-b199-e3d6ffc0b08c', '39');
INSERT INTO `visitor` VALUES ('34', '1', '来访朋友', '黄天疆', '15883794464', '507', '2020-10-27', '1', null, '9bb8c487-b42b-413c-b363-217e21155f30', '39');
INSERT INTO `visitor` VALUES ('64', '1', '发钱了', '蔡权位', '19875602233', '110', '2020-11-02', '1', '川Q66666', 'a67e07e9-c8cf-4a14-a6b4-32cd0cad2afe', '54');
INSERT INTO `visitor` VALUES ('70', '2', '拜访', 'test', '19875602233', '301', '2020-11-03', '3', '皖C22331', '503695cf-8c95-447b-aff6-9bf7112851c5', '54');
INSERT INTO `visitor` VALUES ('71', '2', '拜访', 'test', '19875602233', '333', '2020-11-03', '3', '沪L33210', '1e97ee41-a87a-4f6e-92d6-03aa294afc49', '54');
INSERT INTO `visitor` VALUES ('72', '1', '拜访', 'test', '19875602233', '102', '2020-11-04', '1', '津D33333', '44fa123d-a96d-49b7-8ed9-cdf7dd38254d', '54');
INSERT INTO `visitor` VALUES ('73', '2', '拜访', 'test', '19875602233', '402', '2020-11-05', '3', '津C22331', 'd2d63c82-0789-463f-90b2-7ab763bba8d3', '54');
INSERT INTO `visitor` VALUES ('74', '1', '', '蔡权位', '19875602288', '110', '2020-11-10', '1', '', 'ebd2a670-ef2b-439b-b59d-ca668809ba54', '54');
